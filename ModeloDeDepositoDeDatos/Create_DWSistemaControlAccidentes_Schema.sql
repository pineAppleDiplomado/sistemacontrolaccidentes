USE [DWSistemaControlAccidentes];
GO

CREATE SCHEMA [ETL];
GO

CREATE TABLE [dbo].[DimArea](
    [AreaID] [int] NOT NULL,
    [NombreProyecto] [nvarchar](50) NOT NULL,
    [Descripcion] [nvarchar](200) NOT NULL,
    [NombreActividad] [nvarchar] (50) NOT NULL
 CONSTRAINT [PK_DimArea] PRIMARY KEY CLUSTERED 
(
    [AreaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[DimNormaSeguridad](
    [NormaSeguridadID] [int] NOT NULL,
    [NSTitulo] [nvarchar](50) NOT NULL,
    [Descripcion] [nvarchar](200) NOT NULL,
    [NSItem] [nvarchar] (150) NOT NULL
 CONSTRAINT [PK_DimNormasSeguridad] PRIMARY KEY CLUSTERED 
(
    [NormaSeguridadID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[DimAccidente](
    [AccidenteID] [int] NOT NULL,
    [NombreAccidente] [varchar](50) NOT NULL,
	[FechaAccidente] [datetime] NOT NULL,
 CONSTRAINT [PK_DimAccidente] PRIMARY KEY CLUSTERED 
(
    [AccidenteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[DimEquipoAsignado](
    [EquipoAsignadoID] [int] NOT NULL,
    [NombreEquipo] [varchar](50) NOT NULL,
	[TipoEquipo] [varchar](50) NOT NULL,
	[NombreTrabajador] [varchar](100) NOT NULL,
	[Rol] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DimEquipoAsignado] PRIMARY KEY CLUSTERED 
(
    [EquipoAsignadoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[FactAccidentesOcurridos](
    [AreaID] [int] NOT NULL,
	[NormasSeguridadID] [int] NOT NULL,
	[AccidenteID] [int] NOT NULL,
	[EquipoAsignadoID] [int] NOT NULL,	
 CONSTRAINT [PK_FactAccidentesOcurridos] PRIMARY KEY CLUSTERED 
(
	[AreaID] ASC,
	[NormasSeguridadID] ASC,
	[AccidenteID] ASC,
	[EquipoAsignadoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [ETL].[Area](
    [AreaID] [int] NOT NULL,
    [NombreProyecto] [nvarchar](50) NOT NULL,
    [Descripcion] [nvarchar](200) NOT NULL,
    [NombreActividad] [nvarchar] (50) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [ETL].[NormaSeguridad](
    [NormaSeguridadID] [int] NOT NULL,
    [NSTitulo] [nvarchar](50) NOT NULL,
    [Descripcion] [nvarchar](200) NOT NULL,
    [NSItem] [nvarchar](150) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [ETL].[Accidente](
    [AccidenteID] [int] NOT NULL,
    [NombreAccidente] [varchar](50) NOT NULL,
	[FechaAccidente] [datetime] NOT NULL,
) ON [PRIMARY]
GO

CREATE TABLE [ETL].[EquipoAsignado](
	[EquipoAsignadoID] [int] NOT NULL,
    [NombreEquipo] [varchar](50) NOT NULL,
	[TipoEquipo] [varchar](50) NOT NULL,
	[NombreTrabajador] [varchar](100) NOT NULL,
	[Rol] [varchar](50) NOT NULL,
) ON [PRIMARY]
GO

CREATE TABLE [ETL].[AccidentesOcurridos](
	[AreaID] [int] NOT NULL,
	[NormasSeguridadID] [int] NOT NULL,
	[AccidenteID] [int] NOT NULL,
	[EquipoAsignadoID] [int] NOT NULL,	
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FactAccidentesOcurridos]  WITH CHECK ADD  CONSTRAINT [FK_DimArea] FOREIGN KEY([AreaID])
REFERENCES [dbo].[DimArea] ([AreaID])
GO
ALTER TABLE [dbo].[FactAccidentesOcurridos] CHECK CONSTRAINT [FK_DimArea]
GO
ALTER TABLE [dbo].[FactAccidentesOcurridos]  WITH CHECK ADD  CONSTRAINT [FK_DimNormaSeguridad] FOREIGN KEY([NormasSeguridadID])
REFERENCES [dbo].[DimNormaSeguridad] ([NormaSeguridadID])
GO
ALTER TABLE [dbo].[FactAccidentesOcurridos] CHECK CONSTRAINT [FK_DimNormaSeguridad]
GO
ALTER TABLE [dbo].[FactAccidentesOcurridos]  WITH CHECK ADD  CONSTRAINT [FK_DimAccidente] FOREIGN KEY([AccidenteID])
REFERENCES [dbo].[DimAccidente] ([AccidenteID])
GO
ALTER TABLE [dbo].[FactAccidentesOcurridos] CHECK CONSTRAINT [FK_DimAccidente]
GO
ALTER TABLE [dbo].[FactAccidentesOcurridos]  WITH CHECK ADD  CONSTRAINT [FK_DimEquipoAsignado] FOREIGN KEY([EquipoAsignadoID])
REFERENCES [dbo].[DimEquipoAsignado] ([EquipoAsignadoID])
GO
ALTER TABLE [dbo].[FactAccidentesOcurridos] CHECK CONSTRAINT [FK_DimEquipoAsignado]
GO

/******************************************************************************
**  Name: ETL.DW_MergeArea
**  Desc: Merges Source ETL.Area changes into Destination dbo.DimArea table. 
**  Called By SQL Job ETL
**
**  Author: Ana Medrano
**
**  Created: 09/02/2019
*******************************************************************************
**                            Change History
*******************************************************************************
**  Date:       Author:          Descripcion:
**  --------    --------------   ---------------------------------------------------
** 09/02/2019   Ana Medrano       Initial Version
******************************************************************************/
CREATE PROCEDURE [ETL].[DW_MergeArea]
AS
SET NOCOUNT ON;
SET XACT_ABORT ON;
BEGIN
    MERGE [dbo].[DimArea] AS target
    USING [ETL].[Area] AS source
    ON
    (
      target.[AreaID] = source.[AreaID]
    )
    WHEN MATCHED
    THEN UPDATE 
         SET [NombreProyecto] = source.[NombreProyecto]
            ,[Descripcion] = source.[Descripcion]
            ,[NombreActividad] = source.[Descripcion]
    WHEN NOT MATCHED
    THEN 
      INSERT
      (
         [AreaID]
        ,[NombreProyecto]
        ,[Descripcion]
        ,[NombreActividad]
      )
      VALUES
      (
        source.[AreaID]
        ,source.[NombreProyecto]
        ,source.[Descripcion]
        ,source.[NombreActividad]
      );
END
GO

/******************************************************************************
**  Name: ETL.DW_MergeNormaSeguridad
**  Desc: Merges Source ETL.NormaSeguridad changes into Destination dbo.DimNormaSeguridad table. 
**  Called By SQL Job ETL
**
**  Author: Ana Medrano
**
**  Created: 09/02/2019
*******************************************************************************
**                            Change History
*******************************************************************************
**  Date:       Author:          Description:
**  --------    --------------   ---------------------------------------------------
** 09/04/2018   Ana Medrano       Initial Version
******************************************************************************/
CREATE PROCEDURE [ETL].[DW_MergeNormaSeguridad]
AS
SET NOCOUNT ON;
SET XACT_ABORT ON;
BEGIN
    MERGE [dbo].[DimNormaSeguridad] AS target
    USING [ETL].[NormaSeguridad] AS source
    ON
    (
      target.[NormaSeguridadID] = source.[NormaSeguridadID]
    )
    WHEN MATCHED
    THEN UPDATE 
         SET  [NSTitulo] = source.[NSTitulo]
              ,[Descripcion] = source.[Descripcion]
              ,[NSItem] = source.[NSItem]
    WHEN NOT MATCHED
    THEN 
      INSERT
      (
        [NormaSeguridadID]
        ,[NSTitulo]
        ,[Descripcion]
        ,[NSItem]
      )
      VALUES
      (
        source.[NormaSeguridadID]
        ,source.[NSTitulo]
        ,source.[Descripcion]
        ,source.[NSItem]
      );
END
GO

/******************************************************************************
**  Name: ETL.DW_MergeAccidente
**  Desc: Merges Source ETL.Accidente changes into Destination dbo.DimAccidente table. 
**  Called By SQL Job ETL
**
**  Author: Ana Medrano
**
**  Created: 09/02/2019
*******************************************************************************
**                            Change History
*******************************************************************************
**  Date:       Author:          Description:
**  --------    --------------   ---------------------------------------------------
** 09/02/2019   Ana Medrano       Initial Version
******************************************************************************/
CREATE PROCEDURE [ETL].[DW_MergeAccidente]
AS
SET NOCOUNT ON;
SET XACT_ABORT ON;
BEGIN
    MERGE [dbo].[DimAccidente] AS target
    USING [ETL].[Accidente] AS source
    ON
    (
      target.[AccidenteID] = source.[AccidenteID]
    )
    WHEN MATCHED
    THEN UPDATE 
         SET [NombreAccidente] = source.[NombreAccidente],
			 [FechaAccidente] = source.[FechaAccidente]		 
    WHEN NOT MATCHED
    THEN 
      INSERT
      (
        [AccidenteID]
        ,[NombreAccidente]
		,[FechaAccidente]
      )
      VALUES
      (
        source.[AccidenteID]
        ,source.[NombreAccidente]
		,source.[FechaAccidente]
      );
END
GO


/******************************************************************************
**  Name: ETL.DW_MergeEquipoAsignado
**  Desc: Merges Source ETL.EquipoAsignado changes into Destination dbo.DimEquipoAsignado table. 
**  Called By SQL Job ETL
**
**  Author: Emilson Rodriguez
**
**  Created: 09/02/2019
*******************************************************************************
**                            Change History
*******************************************************************************
**  Date:       Author:          Description:
**  --------    --------------   ---------------------------------------------------
** 09/02/2019   Emilson Rodriguez  Initial Version
******************************************************************************/
CREATE PROCEDURE [ETL].[DW_EquipoAsignado]
AS
SET NOCOUNT ON;
SET XACT_ABORT ON;
BEGIN
    MERGE [dbo].[DimEquipoAsignado] AS target
    USING [ETL].[EquipoAsignado] AS source
    ON
    (
      target.[EquipoAsignadoID] = source.[EquipoAsignadoID]
    )
    WHEN MATCHED
    THEN UPDATE 
         SET [NombreEquipo] = source.[NombreEquipo]
		 ,[TipoEquipo] = source.[TipoEquipo]
		 ,[NombreTrabajador] = source.[NombreTrabajador]
		 ,[Rol] = source.[NombreTrabajador]
	WHEN NOT MATCHED
    THEN 
      INSERT
      (
        [EquipoAsignadoID]
        ,[NombreEquipo]
		,[TipoEquipo]
		,[NombreTrabajador]
		,[Rol]
      )
      VALUES
      (
        source.[EquipoAsignadoID]
        ,source.[NombreEquipo]
		,source.[TipoEquipo]
		,source.[NombreTrabajador]
		,source.[Rol]
      );
END
GO

/******************************************************************************
**  Name: ETL.DW_MergeAccidenteOcurrido
**  Desc: Merges Source ETL.AccidenteOcurrido changes into Destination dbo.DimAccidenteOcurrido table. 
**  Called By SQL Job ETL
**
**  Author: Emilson Rodriguez
**
**  Created: 09/02/2019
*******************************************************************************
**                            Change History
*******************************************************************************
**  Date:       Author:          Description:
**  --------    --------------   ---------------------------------------------------
** 09/02/2019   Emilson Rodriguez  Initial Version
******************************************************************************/
CREATE PROCEDURE [ETL].[DW_AccidenteOcurrido]
AS
SET NOCOUNT ON;
SET XACT_ABORT ON;
BEGIN
    MERGE [dbo].[FactAccidenteOcurrido] AS target
    USING [ETL].[AccidenteOcurrido] AS source
    ON
	(
	  target.[AreaID]     = source.[AreaID]
	  AND target.[NormasSeguridadID]     = source.[NormasSeguridadID]
	  AND target.[AccidenteID] = source.[AccidenteID]
	  AND target.[EquipoAsignadoID]    = source.[EquipoAsignadoID]
	)    
	WHEN NOT MATCHED
    THEN 
      INSERT
      (
        [AreaID]
        ,[NormasSeguridadID]
		,[AccidenteID]
		,[EquipoAsignadoID]
	)
      VALUES
      (
        source.[AreaID]
        ,source.[NormasSeguridadID]
		,source.[AccidenteID]
		,source.[EquipoAsignadoID]
		);
END
GO