USE Seguridad;

/*
*  Procedimiento Almacenado Asignar un trabajador a un accidente ocurrido, con detalle.
*  Observaciones: En la primera version no existia el atributo DescripcionAccidenteOcurrido
*                 en esta version si!
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   17/02/2019     Implementacion V.Final
*/

PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Asignar_Trabajador_a_Accidente]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[Asignar_Trabajador_a_Accidente]
END
GO

CREATE PROCEDURE [dbo].[Asignar_Trabajador_a_Accidente]
(
	@IdTrabajador INT,
	@IdAccidente INT,
	@detalleAccidente varchar(300)
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;

DECLARE @existe INT;
DECLARE @IdAsignacion INT;
DECLARE @IdGeneradoAccidenteOcurrido INT; 



--verificando id de trabajador: nulo, vacio, numerico, existencia en Entidad Trabajdor
IF(@IdTrabajador IS NULL) begin
	PRINT 'El Id de trabajador no puede ser Nulo';
end
ELSE begin
	IF(@IdTrabajador = '') begin
		PRINT 'El Id de trabajador no puede estar vacio';
	end
	ELSE begin
		IF(ISNUMERIC(@IdTrabajador) <> 1) begin
			PRINT 'Tipo de dato idTrabajador incorrecto, ingrese un entero!';
		end
		ELSE begin

			--ver si existe el Id de trabajdor asignado a una tarea
			SELECT @existe = asignacion.IDTrabajador
			FROM Asignacion asignacion
			WHERE asignacion.IDTrabajador = @IdTrabajador

			IF(@existe IS NULL) begin
				PRINT 'No existe el trabajador asignado a una tarea!';
			end
			ELSE begin
				--verificando Id de accidente: nulo, vacio, numerico, existencia en Entidad Accidente

						IF(@IdAccidente IS NULL) begin
							PRINT 'El Id de accidente no puede ser Nulo';
						end
						ELSE begin
							IF(@IdAccidente = '') begin
								PRINT 'El Id de accidente no puede estar vacio';
							end
							ELSE begin
								IF(ISNUMERIC(@IdAccidente) <> 1) begin
									PRINT 'Tipo de dato idAccidente incorrecto, ingrese un entero!';
								end
								ELSE begin

									DECLARE @existe2 INT;
									--ver si existe el Id de la accidente en entidad Accidente
									SELECT @existe2 = accidente.IDAccidente
									FROM Accidente accidente
									WHERE accidente.IDAccidente = @IdAccidente

									IF(@existe2 IS NULL) begin
										PRINT 'No existe el IdAccidente en la entidad Accidentes!';
									end
									ELSE begin


										/*INSERTANDO A 3 ENTIDADES, AccidenteOcurrido,
																	DetalleAccidenteOcurrido y 
																	AssignacionAccidenteOcurrido
										*/

										--Entidad AccidenteOcurrido
										    DECLARE @fecha DATETIME = GETDATE();
											INSERT INTO [dbo].[AccidenteOcurrido]([Fecha], [IDAccidente],[IDActividadDetalle])
											VALUES (@fecha, @IdAccidente, NULL)
											SET @IdGeneradoAccidenteOcurrido = @@IDENTITY;

										--Entidad DetalleAccidenteOcurrido
											INSERT INTO [dbo].[DetalleAccidenteOcurrido]([IDAccidenteOcurrido],[DescripcionAccidenteOcurrido])
											VALUES (@IdGeneradoAccidenteOcurrido, @detalleAccidente);
											--INSERT INTO [dbo].[DetalleAccidenteOcurrido]([IDAccidenteOcurrido])
											--VALUES (@IdGeneradoAccidenteOcurrido);

										--Entidad AssignacionAccidenteOcurrido	    --SUPONIENDO QUE TRABAJADOR SOLO TIENE UNA ASIGNACION DE TRABAJO. BANDERA PARA TENER OTRAS.
											SELECT @IdAsignacion = asignacion.IDAsignacion
											FROM Asignacion asignacion
											WHERE asignacion.IDTrabajador = @IdTrabajador

											INSERT INTO [dbo].[AsignacionAccidenteOcurrido]([IDAsignacion],[IDAccidenteOcurrido])
											VALUES (@IdAsignacion, @IdGeneradoAccidenteOcurrido);
											PRINT 'Se asigno el accidente y su detalle al trabajador';
									end
								end
							end
						end
					end
				end
			end
		end

PRINT 'Procedure [dbo].[Asignar_Trabajador_a_Accidente] created';
GO

-------------------------------------------------------------------------------

--idTrabajodor, idAccidente, 'detalle'
--EXECUTE [dbo].[Asignar_Trabajador_a_Accidente] 1, 1, 'Se cayo del 3er piso, se rompio el brazo, pero aun vive';
