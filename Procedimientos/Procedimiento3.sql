USE Seguridad;

/*
*  Procedimiento Almacenado Asignar un Trabajador a una Actividad en Ejecucion por Proyecto.
*
*
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   17/02/2019     Implementacion V. Final
*/


PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Asignar_Trabajador_a_Actividad]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[Asignar_Trabajador_a_Actividad]
END
GO

CREATE PROCEDURE [dbo].[Asignar_Trabajador_a_Actividad]
(
	@IdTrabajador INT,
	@IdActividad INT,
	@IdProyecto INT
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;

DECLARE @existe int;
DECLARE @idDetalle int;

--verificando id de trabajador: nulo, vacio, numerico, existencia en Entidad Trabajdor

IF(@IdTrabajador IS NULL) begin
	PRINT 'El Id de trabajador no puede ser Nulo';
end
ELSE begin
	IF(@IdTrabajador = '') begin
		PRINT 'El Id de trabajador no puede estar vacio';
	end
	ELSE begin
		IF(ISNUMERIC(@IdTrabajador) <> 1) begin
			PRINT 'Tipo de dato idTrabajador incorrecto, ingrese un entero!';
		end
		ELSE begin

			--ver si existe el Id de trabajdor
			SELECT @existe = trabajador.IDTrabajador
			FROM Trabajador trabajador
			WHERE trabajador.IDTrabajador = @IdTrabajador

			IF(@existe IS NULL) begin
				PRINT 'No existe el trabajador';
			end
			ELSE begin

				DECLARE @existe2 int;
				--verificando id de proyecto: nulo, vacio, numerico, existencia
				IF(@IdProyecto IS NULL) begin
					PRINT 'El Id de proyecto no puede ser Nulo';
				end
				ELSE begin
					IF(@IdProyecto = '') begin
						PRINT 'El Id de proyecto no puede estar vacio';
					end
					ELSE begin
						IF(ISNUMERIC(@IdProyecto) <> 1) begin
							PRINT 'Tipo de dato idProyecto incorrecto, ingrese un entero!';
						end
						ELSE begin

							--ver si existe el Id de proyecto
							SELECT @existe2 = proyecto.[IDProyecto]
							FROM Proyecto proyecto
							WHERE proyecto.[IDProyecto] = @IdProyecto

							IF(@existe2 IS NULL) begin
								PRINT 'No existe el proyecto';
							end
							ELSE begin


								--verificando Id de actividad: nulo, vacio, numerico, existencia en Entidad ActividadDetalle
								DECLARE @existe3 int;
								IF(@IdActividad IS NULL) begin
									PRINT 'El Id de actividad no puede ser Nulo';
								end
								ELSE begin
									IF(@IdActividad = '') begin
										PRINT 'El Id de actividad no puede estar vacio';
									end
									ELSE begin
										IF(ISNUMERIC(@IdActividad) <> 1) begin
											PRINT 'Tipo de dato idActividad incorrecto, ingrese un entero!';
										end
										ELSE begin

											--ver si existe el Id de la actividad en ActividadDetalle, agarrar su IdDetalle
											--e insertar en entidad Asignar.
											SELECT @existe3 = actDetalle.[IDActividad], @idDetalle = actDetalle.[IDActividadDetalle]
											FROM ActividadDetalle actDetalle
											WHERE actDetalle.[IDActividad] = @IdActividad and
												  actDetalle.[IDProyecto] = @IdProyecto

											IF(@existe3 IS NULL) begin
												PRINT 'No existe la actividad';
											end
											ELSE begin
												INSERT INTO [dbo].[Asignacion]([IDActividadDetalle], [IDTrabajador])
												VALUES (@idDetalle, @IdTrabajador);
												PRINT 'Trabajador asignado a una Actividad y a un Proyecto exitosamente!';
											end
										end
									end
								end
							end
						end
					end
				end
			end
		end
	end
end

PRINT 'Procedure [dbo].[Asignar_Trabajador_a_Actividad] created';
GO

----------------------------Fin Asignar trabajador a Actividad---------

--idTrabajodor, idActividad, idProyecto
--EXECUTE [dbo].[Asignar_Trabajador_a_Actividad] 9, 1, null;
