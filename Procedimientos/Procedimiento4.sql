USE Seguridad;

/*
*  Procedimiento Almacenado Asignar un equipo a un trabajador que tiene 
                            actividad asignada en Ejecucion.
*
*
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   16/02/2019     Implementacion V.Final
*/

PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Asignar_Equipo_a_Trabajador]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[Asignar_Equipo_a_Trabajador]
END
GO

CREATE PROCEDURE [dbo].[Asignar_Equipo_a_Trabajador]
(
	@IdTrabajador INT,
	@IdEquipo INT,
	@IdActividad INT,
	@IdProyecto INT
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;

DECLARE @existe INT;
DECLARE @idDetalle INT;
DECLARE @IdAsignacion INT


--verificando id de trabajador: nulo, vacio, numerico, existencia en Entidad Trabajdor
IF(@IdTrabajador IS NULL) begin
	PRINT 'El Id de trabajador no puede ser Nulo';
end
ELSE begin
	IF(@IdTrabajador = '') begin
		PRINT 'El Id de trabajador no puede estar vacio';
	end
	ELSE begin
		IF(ISNUMERIC(@IdTrabajador) <> 1) begin
			PRINT 'Tipo de dato idTrabajador incorrecto, ingrese un entero!';
		end
		ELSE begin

			--ver si existe el Id de trabajdor
			SELECT @existe = trabajador.IDTrabajador
			FROM Trabajador trabajador
			WHERE trabajador.IDTrabajador = @IdTrabajador

			IF(@existe IS NULL) begin
				PRINT 'No existe el trabajador';
			end
			ELSE begin
				

				DECLARE @existe2 INT;
				--verificando id de Equipo: nulo, vacio, numerico, existencia en Entidad Equipo
				IF(@IdEquipo IS NULL) begin
					PRINT 'El Id de Equipo no puede ser Nulo';
				end
				ELSE begin
					IF(@IdEquipo = '') begin
						PRINT 'El Id de Equipo no puede estar vacio';
					end
					ELSE begin
						IF(ISNUMERIC(@IdEquipo) <> 1) begin
							PRINT 'Tipo de dato idEquipo incorrecto, ingrese un entero!';
						end
						ELSE begin

							--ver si existe el Id de equipo
							SELECT @existe2 = equipo.[IDEquipo]
							FROM Equipo equipo
							WHERE equipo.[IDEquipo] = @IdEquipo

							IF(@existe2 IS NULL) begin
								PRINT 'No existe el Equipo';
							end
							ELSE begin


								DECLARE @existe3 INT;
								--verificando id de proyecto: nulo, vacio, numerico, existencia
								IF(@IdProyecto IS NULL) begin
									PRINT 'El Id de proyecto no puede ser Nulo';
								end
								ELSE begin
									IF(@IdProyecto = '') begin
										PRINT 'El Id de proyecto no puede estar vacio';
									end
									ELSE begin
										IF(ISNUMERIC(@IdProyecto) <> 1) begin
											PRINT 'Tipo de dato idProyecto incorrecto, ingrese un entero!';
										end
										ELSE begin

											--ver si existe el Id de proyecto
											SELECT @existe3 = proyecto.[IDProyecto]
											FROM Proyecto proyecto
											WHERE proyecto.[IDProyecto] = @IdProyecto

											IF(@existe3 IS NULL) begin
												PRINT 'No existe el proyecto';
											end
											ELSE begin



												--verificando Id de actividad: nulo, vacio, numerico, existencia en Entidad ActividadDetalle
												DECLARE @existe4 INT;
												IF(@IdActividad IS NULL) begin
													PRINT 'El Id de actividad no puede ser Nulo';
												end
												ELSE begin
													IF(@IdActividad = '') begin
														PRINT 'El Id de actividad no puede estar vacio';
													end
													ELSE begin
														IF(ISNUMERIC(@IdActividad) <> 1) begin
															PRINT 'Tipo de dato idActividad incorrecto, ingrese un entero!';
														end
														ELSE begin

															--ver si existe el Id de la actividad en Actividad
															SELECT @existe4 = act.[IDActividad]
															FROM Actividad act
															WHERE act.[IDActividad] = @IdActividad

															IF(@existe4 IS NULL) begin
																PRINT 'No existe la actividad';
															end
															ELSE begin


																--verificando que el trabajador tenga una actividad asignada en un proyecto
																SELECT @IdAsignacion = asignacion.IDAsignacion
																FROM Asignacion asignacion INNER JOIN ActividadDetalle actDetalle
																						   ON (asignacion.IDActividadDetalle = actDetalle.IDActividadDetalle)
																WHERE asignacion.IDTrabajador = @IdTrabajador and
																	  actDetalle.IDActividad = @IdActividad   and
																	  actDetalle.IDProyecto = @IdProyecto

																IF(@IdAsignacion IS NULL) begin
																	PRINT 'No existe una asignacion del trabajador con la actividad';
																end
																ELSE begin
																	INSERT INTO [dbo].[EquipoAsignado]([IDEquipo], [IDAsignacion])
																	VALUES (@IdEquipo, @IdAsignacion);
																	PRINT 'Equipo asignado a trabajador en dicha actividad exitosamente!';
																end
															end
														end
													end
												end
											end
										end
									end
								end
							end
						end
					end
				end
			end
		end
	end
end

PRINT 'Procedure [dbo].[Asignar_Equipo_a_Trabajador] created';
GO

-------------------------------------------------------------------------

--idTrabajodor, idEquipo, idActividad, idProyecto
--EXECUTE [dbo].[Asignar_Equipo_a_Trabajador] 6, 13, 1, 3;