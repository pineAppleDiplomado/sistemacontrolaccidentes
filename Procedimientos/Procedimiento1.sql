USE Seguridad;

/*
*  Procedimiento Almacenado Insertar Trabajador

*  Nombre                 Fecha                   Description
*  ---------------------------------------------------------------------
*  Javier Zapata Candia   16/02/2019         Implementacion Version Final
*/


PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[InsertarTrabajador]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[InsertarTrabajador]
END
GO

CREATE PROCEDURE [dbo].[InsertarTrabajador]
(
	@Apellidos VARCHAR(50),
	@Nombres VARCHAR(50),
	@fecha DATETIME,
	@IdRol INT
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;


DECLARE @existe int;
IF(@Nombres = '') begin
	PRINT 'El Nombre no puede estar vacio!';
end
ELSE begin
	IF(@Nombres IS NULL) begin
		PRINT 'El Nombre no puede ser nulo!';
	end
	ELSE begin
		IF(@Nombres LIKE '%[0-9]%') begin
			PRINT 'El Nombre no puede contener numeros!';
		end
		ELSE begin

------
			IF(@Apellidos = '') begin
				PRINT 'El Apellido no puede estar vacio!';
			end
			ELSE begin
				IF(@Apellidos IS NULL) begin
					PRINT 'El Apellido no puede ser nulo!';
				end
				ELSE begin
					IF(@Apellidos LIKE '%[0-9]%') begin
						PRINT 'El Apellido no puede contener numeros';
					end
					ELSE begin

------
								IF(@IdRol IS NULL) begin
									PRINT 'El rol no puede ser nulo';
								end
								ELSE begin
									IF(@IdRol = '') begin
										PRINT 'El rol no puede estar vacio';
									end
									ELSE begin
										IF(@IdRol NOT LIKE '%[0-9]%') begin
											PRINT 'El rol solo debe contener numeros';
										end
										ELSE begin

											--ver si existe en Entidad Rol  
											SELECT @existe = rol.IDRol
											FROM Rol rol
											WHERE rol.IDRol = @IdRol

											IF(@existe IS NULL) begin
												PRINT 'No existe ese codigo de Rol!';
											end
											ELSE begin
												INSERT INTO [dbo].[Trabajador](Apellidos, Nombres, FechaContratacion, IDRol)
												VALUES (@Apellidos, @Nombres, @fecha, @IdRol);
												PRINT 'Trabajador insertado exitosamente!'
											end
										end
									end
								end
							end
						end
					end
				end
			end
		end

PRINT 'Procedure [dbo].[InsertarTrabajador] created';
GO

-------------------------------------------FIN INSERT TRABAJADOR -----------------------

--nombres, apellidos, fecha, rol
--DECLARE @fecha DATETIME = GETDATE();
--EXECUTE [dbo].[InsertarTrabajador] 'Lopez', 'Lucho', @fecha, 9;
--go
