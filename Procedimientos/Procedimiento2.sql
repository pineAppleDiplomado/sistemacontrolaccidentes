USE Seguridad;

/*
*  Procedimiento Almacenado Update del Rol de un trabajador

*  Nombre                 Fecha                   Description
*  ---------------------------------------------------------------------
*  Javier Zapata Candia   16/02/2019         Implementacion Version Final
*/

PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Update_Rol_De_Trabajador]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[Update_Rol_De_Trabajador]
END
GO

CREATE PROCEDURE [dbo].[Update_Rol_De_Trabajador]
(
	@IdTrabajador INT,
	@IdRol INT
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;

DECLARE @existe int;
IF(@IdTrabajador IS NULL) begin
	PRINT 'El Id de Trabajador no puede ser Nulo';
end
ELSE begin
	IF(@IdTrabajador = '') begin
		PRINT 'El Id de Trabajador no puede estar vacio';
	end
	ELSE begin
		IF(ISNUMERIC(@IdTrabajador) <> 1) begin
			PRINT 'Tipo de dato id de Trabajador incorrecto, ingrese un entero!';
		end	
		ELSE begin
			--ver si existe el id de trabajador
			SELECT @existe = trabajador.IDTrabajador
			FROM Trabajador trabajador
			WHERE trabajador.IDTrabajador = @IdTrabajador

			IF(@existe IS NULL) begin
				PRINT 'El trabajador no existe';
			end

			ELSE begin
				IF(@IdRol IS NULL) begin
					PRINT 'El Id de rol no puede ser Nulo';
				end
				ELSE begin
					IF(@IdRol = '') begin
						PRINT 'El Id de rol no puede estar vacio';
					end
					ELSE begin
						IF(ISNUMERIC(@IdRol) <> 1) begin
							PRINT 'Tipo de dato idRol incorrecto, ingrese un entero!';
						end

						ELSE begin

							--ver si el rol existe en entidad Rol
							DECLARE @existe2 INT;
							SELECT @existe2 = rol.IDRol
							FROM Rol rol
							WHERE rol.IDRol = @IdRol

							IF(@existe2 IS NULL) begin
								PRINT 'El codigo de Rol no existe';
							end
							ELSE begin
								UPDATE [Trabajador]
								SET  IDRol = @IdRol
								WHERE IDTrabajador = @IdTrabajador
								PRINT 'Se actualizo el rol del trabajador!';
							end
						end
					end
				end
			end
		end
	end
end
GO

PRINT 'Procedure [dbo].[Update_Rol_De_Trabajador] created';
GO


-------------------------------------FIN DE UPDATE ROL DE UN TRABAJADOR -----------

--idTrabajador, idRol
--EXECUTE [dbo].[Update_Rol_De_Trabajador] 16, 8;

