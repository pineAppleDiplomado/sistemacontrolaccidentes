
USE Seguridad;
go

/*
*  Auditando la entidad [Trabajador].
*
* 
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   17/02/2019     Implementacion V. Final
*/



IF EXISTS (SELECT * FROM sys.triggers 
		   WHERE type = 'TR' AND object_id = OBJECT_ID(N'[dbo].[TG_Trabajador_Insert_Update]'))
BEGIN
	DROP TRIGGER [dbo].[TG_Trabajador_Insert_Update]
END
GO


CREATE TRIGGER [dbo].[TG_Trabajador_Insert_Update]
ON [dbo].[Trabajador]

AFTER INSERT, UPDATE
AS
BEGIN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
  DECLARE @Action VARCHAR(20) = '';

  SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED) THEN 'Update'
					  WHEN EXISTS(SELECT * FROM INSERTED) THEN 'Insert'
			    end);
  PRINT @Action; 
  IF(@Action = 'Update') begin

	IF UPDATE (Apellidos) begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'Trabajador', 
			   Campo_Actualizado = 'Apellidos',
				Id_Registro        = d.[IDTrabajador], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = d.[Apellidos], 
				Valor_nuevo     = i.[Apellidos],
				Tipo_Operacion = 'Update',
				ModificadoPor   =  USER_NAME()
		FROM  Inserted i, Deleted d	
		PRINT 'Se actualizo los APELLIDOS del trabajador ..';        
    end

	ELSE begin

		IF UPDATE ([Nombres]) begin
			INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
			SELECT Nombre_de_Tabla    = 'Trabajador', 
				   Campo_Actualizado = 'Nombres',
					Id_Registro        = d.[IDTrabajador], 
					Fecha_Realizada       = GETDATE(), 
					Valor_antiguo     = d.[Nombres], 
					Valor_nuevo     = i.[Nombres],
					Tipo_Operacion = 'Update',
					ModificadoPor   =  USER_NAME()
			FROM  Inserted i, Deleted d	
			PRINT 'Se actualizo los NOMBRES del trabajador ..';        
		end

		ELSE begin
			IF UPDATE ([IDRol]) begin
					INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
					SELECT Nombre_de_Tabla    = 'Trabajador', 
						   Campo_Actualizado = 'Rol',
							Id_Registro        = d.[IDTrabajador], 
							Fecha_Realizada       = GETDATE(), 
							Valor_antiguo     = d.[IDRol], 
							Valor_nuevo     = i.[IDRol],
							Tipo_Operacion = 'Update',
							ModificadoPor   =  USER_NAME()
					FROM  Inserted i, Deleted d	
					PRINT 'Se actualizo el Rol del trabajador ..';        
				end
			end
		end
end --if Accion

ELSE begin

	IF(@Action = 'Insert') begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'Trabajador', 
				Campo_Actualizado = 'todo el registro',
				Id_Registro        = i.[IDTrabajador], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = '', 
				Valor_nuevo     = Cast(i.[IDTrabajador] as VARCHAR) +' - '+i.[Nombres]+ ' - '+i.[Apellidos] +' - '+CAST(i.[IDRol] AS VARCHAR),
				Tipo_Operacion = 'Insert',
				ModificadoPor   =  USER_NAME()
		FROM  Inserted i	
		PRINT 'Se inserto un trabajador ..'; 
	end		
end

END
GO

--drop trigger [TG_Trabajador_Insert_Update]
------------------------------------------------------------------------------------


IF EXISTS (SELECT * FROM sys.triggers 
		   WHERE type = 'TR' AND object_id = OBJECT_ID(N'[dbo].[TG_Trabajador_Delete]'))
BEGIN
	DROP TRIGGER [dbo].[TG_Trabajador_Delete]
END
GO


CREATE TRIGGER [dbo].[TG_Trabajador_Delete]
ON [dbo].[Trabajador]

INSTEAD OF DELETE
AS
BEGIN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;

  DECLARE @Action VARCHAR(20) = '';
  SET @Action = 'Delete';

  IF(@Action = 'Delete') begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'Trabajador', 
				Campo_Actualizado = 'tratando de borrar todo el registro',
				Id_Registro        = d.[IDTrabajador], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = '', 
				Valor_nuevo     = '',
				Tipo_Operacion = 'Delete',
				ModificadoPor   =  USER_NAME()
		FROM  Deleted d
		PRINT 'Se cancelo la eliminacion ..';
  end

END
GO


--DROP trigger [TG_Trabajador_Delete]

