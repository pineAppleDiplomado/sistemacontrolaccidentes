
USE Seguridad;
go


/*
*  Creacion de la tabla para Auditoria
*
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   08/02/2019     Implementacion Inicial
*/



CREATE TABLE [dbo].[Historial_Auditoria]
(
	[Id_Auditoria] INT IDENTITY(1,1) NOT NULL CONSTRAINT [PK_AuditHistory] PRIMARY KEY,
	[Nombre_de_Tabla]		 VARCHAR(50) NULL,
	[Campo_Actualizado]	 VARCHAR(50) NULL,
	[Id_Registro]             INT NULL,
	[Fecha_Realizada]           DATETIME NULL,
	[Valor_antiguo]       VARCHAR(MAX) NULL,
	[Valor_nuevo]       VARCHAR(MAX) NULL,
	[Tipo_Operacion] VARCHAR(20) NULL,
	[ModificadoPor]     VARCHAR(50)
);
GO

--drop table [dbo].[Historial_Auditoria]