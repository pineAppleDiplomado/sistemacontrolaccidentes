USE Seguridad;
go

/*
*  Auditando la entidad [EquipoAsignado]. Es importante
*  auditar esta entidad, ya que es de importancia saber que equipos
*  se le asigna al trabajador independientemente de los que 
*  tendria que tener dependiendo de su Rol.

*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   17/02/2019     Implementacion V. Final
*/

IF EXISTS (SELECT * FROM sys.triggers 
		   WHERE type = 'TR' AND object_id = OBJECT_ID(N'[dbo].[TG_Asignar_Equipo_Inserte_Update]'))
BEGIN
	DROP TRIGGER [dbo].[TG_Asignar_Equipo_Inserte_Update]
END
GO


CREATE TRIGGER [dbo].[TG_Asignar_Equipo_Inserte_Update]
ON [dbo].[EquipoAsignado]

AFTER INSERT, UPDATE
AS
BEGIN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
  DECLARE @Action VARCHAR(20) = '';

  SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED) THEN 'Update'
					  WHEN EXISTS(SELECT * FROM INSERTED) THEN 'Insert'
			    end);
  PRINT @Action; 
  IF(@Action = 'Update') begin

	IF UPDATE (IDEquipo) begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'EquipoAsignado', 
			   Campo_Actualizado = 'IDEquipo',
				Id_Registro        = d.[IDEquipoAsignado], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = d.[IDEquipo], 
				Valor_nuevo     = i.[IDEquipo],
				Tipo_Operacion = 'Update',
				ModificadoPor   =  USER_NAME()
		FROM  Inserted i, Deleted d	
		PRINT 'Se actualizo el Id del Equipo ..';        
    end

	ELSE begin

		IF UPDATE (IDAsignacion) begin
			INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
			SELECT Nombre_de_Tabla    = 'EquipoAsignado', 
				   Campo_Actualizado = 'IDAsignacion',
					Id_Registro        = d.[IDEquipoAsignado], 
					Fecha_Realizada       = GETDATE(), 
					Valor_antiguo     = d.[IDAsignacion], 
					Valor_nuevo     = i.[IDAsignacion],
					Tipo_Operacion = 'Update',
					ModificadoPor   =  USER_NAME()
			FROM  Inserted i, Deleted d	
			PRINT 'Se actualizo el Id de Asignacion ..';        
		end
	end
  end

ELSE begin

	IF(@Action = 'Insert') begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'EquipoAsignado', 
				Campo_Actualizado = 'todo el registro',
				Id_Registro        = i.[IDEquipoAsignado], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = '', 
				Valor_nuevo     = Cast(i.[IDEquipo] as VARCHAR) +' - '+CAST(i.[IDAsignacion] as VARCHAR),
				Tipo_Operacion = 'Insert',
				ModificadoPor   =  USER_NAME()
		FROM  Inserted i	
		PRINT 'Se inserto un registro en EquipoAsignado ..'; 
	end		
end

END
GO

--DROP trigger [TG_Asignar_Equipo_Inserte_Update]
------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM sys.triggers 
		   WHERE type = 'TR' AND object_id = OBJECT_ID(N'[dbo].[TG_AsignacionEquipo_Delete]'))
BEGIN
	DROP TRIGGER [dbo].[TG_AsignacionEquipo_Delete]
END
GO


CREATE TRIGGER [dbo].[TG_AsignacionEquipo_Delete]
ON [dbo].[EquipoAsignado]

INSTEAD OF DELETE
AS
BEGIN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;

  DECLARE @Action VARCHAR(20) = '';
  SET @Action = 'Delete';

  IF(@Action = 'Delete') begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'EquipoAsignado', 
				Campo_Actualizado = 'tratando de borrar todo el registro',
				Id_Registro        = d.[IDEquipoAsignado], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = '', 
				Valor_nuevo     = '',
				Tipo_Operacion = 'Delete',
				ModificadoPor   =  USER_NAME()
		FROM  Deleted d
		PRINT 'Se cancelo la eliminacion ..';
  end

END
GO


--DROP trigger [TG_AsignacionEquipo_Delete]