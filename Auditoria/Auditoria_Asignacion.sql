
USE Seguridad;
go

/*
*  Auditando la asignacion de trabajador a la entidad Asignacion,
*  la cual tendra una relacion con equipos asignados y con 
*  asignacion a accidentes ocurridos.

*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   17/02/2019     Implementacion V. Final
*/

IF EXISTS (SELECT * FROM sys.triggers 
		   WHERE type = 'TR' AND object_id = OBJECT_ID(N'[dbo].[TG_Asignar_Trabajador_Asignacion_Insert_Update]'))
BEGIN
	DROP TRIGGER [dbo].[TG_Asignar_Trabajador_Asignacion_Insert_Update]
END
GO

CREATE TRIGGER [dbo].[TG_Asignar_Trabajador_Asignacion_Insert_Update]
ON [dbo].[Asignacion]

AFTER INSERT, UPDATE
AS
BEGIN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
  DECLARE @Action VARCHAR(20) = '';

  SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED) THEN 'Update'
					  WHEN EXISTS(SELECT * FROM INSERTED) THEN 'Insert'
			    end);
  PRINT @Action; 
  IF(@Action = 'Update') begin

	IF UPDATE (IDTrabajador) begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'Asignacion', 
			   Campo_Actualizado = 'ID_Trabajador',
				Id_Registro        = d.[IDAsignacion], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = d.[IDTrabajador], 
				Valor_nuevo     = i.[IDTrabajador],
				Tipo_Operacion = 'Update',
				ModificadoPor   =  USER_NAME()
		FROM  Inserted i, Deleted d	
		PRINT 'Se actualizo el Id del trabajador ..';        
    end

	ELSE begin

		IF UPDATE (IDActividadDetalle) begin
			INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
			SELECT Nombre_de_Tabla    = 'Asignacion', 
				   Campo_Actualizado = 'IDActividadDetalle',
					Id_Registro        = d.[IDAsignacion], 
					Fecha_Realizada       = GETDATE(), 
					Valor_antiguo     = d.[IDActividadDetalle], 
					Valor_nuevo     = i.[IDActividadDetalle],
					Tipo_Operacion = 'Update',
					ModificadoPor   =  USER_NAME()
			FROM  Inserted i, Deleted d	
			PRINT 'Se actualizo el Id de ActividadDetalle ..';        
		end
	end
  end

ELSE begin

	IF(@Action = 'Insert') begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'Asignacion', 
				Campo_Actualizado = 'todo el registro',
				Id_Registro        = i.[IDAsignacion], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = '', 
				Valor_nuevo     = Cast(i.[IDActividadDetalle] as VARCHAR) +' - '+CAST(i.[IDTrabajador] as VARCHAR),
				Tipo_Operacion = 'Insert',
				ModificadoPor   =  USER_NAME()
		FROM  Inserted i	
		PRINT 'Se inserto un registro en Asignacion ..'; 
	end		
end

END
GO

--DROP trigger [TG_Asignar_Trabajador_Asignacion_Insert_Update]
------------------------------------------------------------------------------------


IF EXISTS (SELECT * FROM sys.triggers 
		   WHERE type = 'TR' AND object_id = OBJECT_ID(N'[dbo].[TG_Asignacion_Delete]'))
BEGIN
	DROP TRIGGER [dbo].[TG_Asignacion_Delete]
END
GO


CREATE TRIGGER [dbo].[TG_Asignacion_Delete]
ON [dbo].[Asignacion]

INSTEAD OF DELETE
AS
BEGIN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;

  DECLARE @Action VARCHAR(20) = '';
  SET @Action = 'Delete';

  IF(@Action = 'Delete') begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'Asignacion', 
				Campo_Actualizado = 'tratando de borrar todo el registro',
				Id_Registro        = d.[IDAsignacion], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = '', 
				Valor_nuevo     = '',
				Tipo_Operacion = 'Delete',
				ModificadoPor   =  USER_NAME()
		FROM  Deleted d
		PRINT 'Se cancelo la eliminacion ..';
  end

END
GO

--DROP trigger [TG_Asignacion_Delete]
