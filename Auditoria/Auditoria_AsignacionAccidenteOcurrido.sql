USE Seguridad;
go


/*
*  Auditando AsignacionAccidenteOcurrido. Es importante
*  auditar esta entidad ya que se asociara con datos de la entidad Asignacion
*  y Accidentesocurrido.
*
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   17/02/2019     Implementacion V. Final
*/

IF EXISTS (SELECT * FROM sys.triggers 
		   WHERE type = 'TR' AND object_id = OBJECT_ID(N'[dbo].[TG_Asignar_AccidenteOcurrido_Insert_Update]'))
BEGIN
	DROP TRIGGER [dbo].[TG_Asignar_AccidenteOcurrido_Insert_Update]
END
GO


CREATE TRIGGER [dbo].[TG_Asignar_AccidenteOcurrido_Insert_Update]
ON [dbo].[AsignacionAccidenteOcurrido]

AFTER INSERT, UPDATE
AS
BEGIN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
  DECLARE @Action VARCHAR(20) = '';

  SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED) THEN 'Update'
					  WHEN EXISTS(SELECT * FROM INSERTED) THEN 'Insert'
			    end);
  PRINT @Action; 
  IF(@Action = 'Update') begin

	IF UPDATE (IDAccidenteOcurrido) begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'AsignacionAccidenteOcurrido', 
			   Campo_Actualizado = 'IDAccidenteOcurrido',
				Id_Registro        = d.[IDAsignacionAccidenteOcurrido], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = d.[IDAccidenteOcurrido], 
				Valor_nuevo     = i.[IDAccidenteOcurrido],
				Tipo_Operacion = 'Update',
				ModificadoPor   =  USER_NAME()
		FROM  Inserted i, Deleted d	
		PRINT 'Se actualizo el Id de AccidenteOcurrido ..';        
    end

	ELSE begin

		IF UPDATE (IDAsignacion) begin
			INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
			SELECT Nombre_de_Tabla    = 'AsignacionAccidenteOcurrido', 
				   Campo_Actualizado = 'IDAsignacion',
					Id_Registro        = d.[IDAsignacionAccidenteOcurrido], 
					Fecha_Realizada       = GETDATE(), 
					Valor_antiguo     = d.[IDAsignacion], 
					Valor_nuevo     = i.[IDAsignacion],
					Tipo_Operacion = 'Update',
					ModificadoPor   =  USER_NAME()
			FROM  Inserted i, Deleted d	
			PRINT 'Se actualizo el Id de Asignacion ..';        
		end
	end
  end

ELSE begin

	IF(@Action = 'Insert') begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'AsignacionAccidenteOcurrido', 
				Campo_Actualizado = 'todo el registro',
				Id_Registro        = i.[IDAsignacionAccidenteOcurrido], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = '', 
				Valor_nuevo     = Cast(i.[IDAsignacion] as VARCHAR) +' - '+CAST(i.[IDAccidenteOcurrido] as VARCHAR),
				Tipo_Operacion = 'Insert',
				ModificadoPor   =  USER_NAME()
		FROM  Inserted i	
		PRINT 'Se inserto un registro en AsignacionAccidenteOcurrido ..'; 
	end		
end

END
GO

--DROP trigger [TG_Asignar_AccidenteOcurrido_Insert_Update]
------------------------------------------------------------------------------------


IF EXISTS (SELECT * FROM sys.triggers 
		   WHERE type = 'TR' AND object_id = OBJECT_ID(N'[dbo].[TG_AsignacionAccidenteOcurrido_Delete]'))
BEGIN
	DROP TRIGGER [dbo].[TG_AsignacionAccidenteOcurrido_Delete]
END
GO


CREATE TRIGGER [dbo].[TG_AsignacionAccidenteOcurrido_Delete]
ON [dbo].[AsignacionAccidenteOcurrido]

INSTEAD OF DELETE
AS
BEGIN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;

  DECLARE @Action VARCHAR(20) = ''; 
  SET @Action = 'Delete';

  IF(@Action = 'Delete') begin
		INSERT INTO Historial_Auditoria(Nombre_de_Tabla, Campo_Actualizado, Id_Registro, Fecha_Realizada, Valor_antiguo, Valor_nuevo, Tipo_Operacion, ModificadoPor)
		SELECT Nombre_de_Tabla    = 'AsignacionAccidenteOcurrido', 
				Campo_Actualizado = 'tratando de borrar todo el registro',
				Id_Registro        = d.[IDAsignacionAccidenteOcurrido], 
				Fecha_Realizada       = GETDATE(), 
				Valor_antiguo     = '', 
				Valor_nuevo     = '',
				Tipo_Operacion = 'Delete',
				ModificadoPor   =  USER_NAME()
		FROM  Deleted d
		PRINT 'Se cancelo la eliminacion ..';
  end

END
GO


--DROP trigger [TG_AsignacionAccidenteOcurrido_Delete]