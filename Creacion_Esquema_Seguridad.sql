/*
*	Esquema Seguridad  
*	Desc: Creacion del Esquema Seguridad
*
*	Nombre					Fecha			Descripcion
*	------------------------------------------------------------
*	Ivan Zapata Torrico		07/02/2019		Creacion de tablas del Esquema Seguridad
*/

USE Seguridad;
GO

PRINT 'Creating the Login table....';

IF NOT EXISTS (SELECT 1 FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[Login]') 
		       AND type in (N'U'))
 BEGIN
		CREATE TABLE [dbo].[Login]([IDLogin] [int] IDENTITY(1,1)  NOT NULL,
								   [Usuario] [nvarchar](16)       NOT NULL,
                                   [Pass]    BINARY(64)           NOT NULL,
								   [IDTrabajador] [int]           NOT NULL,
		 CONSTRAINT [PK_Login] PRIMARY KEY
		(
			[IDLogin] ASC
		));

		PRINT 'Table Login created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table Login already exists into the database';
	END
GO


PRINT 'Creating the Trabajador table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Trabajador]') 
		AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[Trabajador]([IDTrabajador] [int] IDENTITY(1,1)  NOT NULL,
										[Apellidos] [nvarchar](50)         NOT NULL,
									    [Nombres] [nvarchar](50)           NOT NULL,
									    [FechaContratacion] [datetime]     NOT NULL,
                                        [IDRol] [int]                      NOT NULL,
		 CONSTRAINT [PK_Trabajador] PRIMARY KEY
		(
			[IDTrabajador] ASC
		));

		PRINT 'Table Trabajador created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table Trabajador already exists into the database';
	END
GO


PRINT 'Creating the Rol table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Rol]') 
		AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[Rol]([IDRol] [int] IDENTITY(1,1)  NOT NULL,
								 [NombreRol] [nvarchar] (50) NOT NULL,
		 CONSTRAINT [PK_Rol] PRIMARY KEY 
		(
			[IDRol] ASC
		));

		PRINT 'Table Rol created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table Rol already exists into the database';
	END
GO


PRINT 'Creating the ManualFunciones table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
			   WHERE object_id = OBJECT_ID(N'[dbo].[ManualFunciones]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[ManualFunciones]([IDManualFunciones] [int] IDENTITY(1,1)  NOT NULL,
											 [Descripcion] [nvarchar] (200) NOT NULL,
		CONSTRAINT [PK_ManualFunciones] PRIMARY KEY
		(
			[IDManualFunciones] ASC
		));

		PRINT 'Table ManualFunciones created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table ManualFunciones already exists into the database';
	END
GO


PRINT 'Creating the AsignacionFuncion table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[AsignacionFuncion]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[AsignacionFuncion]([IDAsignacionFuncion] [int] IDENTITY(1,1)  NOT NULL,
										       [IDRol] [int]                             NOT NULL,
                                               [IDManualFunciones] [int]                 NOT NULL,
		 CONSTRAINT [PK_AsignacionFuncion] PRIMARY KEY 
		(
			[IDAsignacionFuncion] ASC
		));

		PRINT 'Table AsignacionFuncion created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table AsignacionFuncion already exists into the database';
	END
GO


PRINT 'Creating the TipoEquipo table...';

IF NOT EXISTS (SELECT * FROM sys.objects 
			   WHERE object_id = OBJECT_ID(N'[dbo].[TipoEquipo]') 
		       AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TipoEquipo]([IDTipoEquipo] [int] IDENTITY(1,1)  NOT NULL,
									[NombreTipoEquipo] [nvarchar] (50) NOT NULL,

	 CONSTRAINT [PK_TipoEquipo] PRIMARY KEY
	(
		[IDTipoEquipo] ASC
	))

		PRINT 'Table TipoEquipo created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table TipoEquipo already exists into the database';
	END
GO


PRINT 'Creating the Equipo table...';

IF NOT EXISTS (SELECT * FROM sys.objects 
			   WHERE object_id = OBJECT_ID(N'[dbo].[Equipo]') 
			   AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[Equipo]([IDEquipo] [int] IDENTITY(1,1)  NOT NULL,
									[NombreEquipo] [nvarchar] (50) NOT NULL,
									[IDTipoEquipo] [int]     NOT NULL,
		 CONSTRAINT [PK_Equipo] PRIMARY KEY
		(
			[IDEquipo] ASC
		));

		PRINT 'Table Equipo created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table Equipo already exists into the database';
	END
GO


PRINT 'Creating the RolEquipo table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[RolEquipo]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[RolEquipo]([IDRolEquipo] [int] IDENTITY(1,1)  NOT NULL,
									   [IDRol] [int]                     NOT NULL,
                                       [IDEquipo] [int]                  NOT NULL,
		 CONSTRAINT [PK_RolEquipo] PRIMARY KEY 
		(
			[IDRolEquipo] ASC
		));

		PRINT 'Table RolEquipo created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table RolEquipo already exists into the database';
	END
GO


PRINT 'Creating the Proyecto table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[Proyecto]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[Proyecto]([IDProyecto] [int] IDENTITY(1,1)  NOT NULL,
										[NombreProyecto] [nvarchar] (50) NOT NULL,
										[Descripcion] [nvarchar] (200) NOT NULL,
										  
		 CONSTRAINT [PK_Proyecto] PRIMARY KEY 
		(
			[IDProyecto] ASC
		));

		PRINT 'Table Proyecto created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table Proyecto already exists into the database';
	END
GO


PRINT 'Creating the Actividad table...';

IF NOT EXISTS (SELECT * FROM sys.objects 
			  WHERE object_id = OBJECT_ID(N'[dbo].[Actividad]')
		      AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[Actividad]([IDActividad] [int] IDENTITY(1,1)  NOT NULL,
										[NombreActividad] [nvarchar] (50) NOT NULL,

		 CONSTRAINT [PK_Actividad] PRIMARY KEY
		(
			[IDActividad] ASC
		))
		PRINT 'Table Actividad created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table Actividad already exists into the database';
	END
GO


PRINT 'Creating the ActividadDetalle table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[ActividadDetalle]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[ActividadDetalle]([IDActividadDetalle] [int] IDENTITY(1,1)  NOT NULL,
										      [IDActividad] [int]                      NOT NULL,
                                              [IDProyecto] [int]                       NOT NULL,
		 CONSTRAINT [PK_ActividadDetalle] PRIMARY KEY 
		(
			[IDActividadDetalle] ASC
		));

		PRINT 'Table ActividadDetalle created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table ActividadDetalle already exists into the database';
	END
GO


PRINT 'Creating the Asignacion table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[Asignacion]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[Asignacion]([IDAsignacion] [int] IDENTITY(1,1)  NOT NULL,
								        [IDActividadDetalle] [int]         NOT NULL,
                                        [IDTrabajador] [int]               NOT NULL,
		 CONSTRAINT [PK_Asignacion] PRIMARY KEY 
		(
			[IDAsignacion] ASC
		));

		PRINT 'Table Asignacion created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table Asignacion already exists into the database';
	END
GO


PRINT 'Creating the EquipoAsignado table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[EquipoAsignado]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[EquipoAsignado]([IDEquipoAsignado] [int] IDENTITY(1,1)  NOT NULL,
										    [IDEquipo] [int]                       NOT NULL,
                                            [IDAsignacion] [int]                   NOT NULL,
		 CONSTRAINT [PK_EquipoAsignado] PRIMARY KEY 
		(
			[IDEquipoAsignado] ASC
		));

		PRINT 'Table EquipoAsignado created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table EquipoAsignado already exists into the database';
	END
GO


PRINT 'Creating the NormaSeguridad table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[NormaSeguridad]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[NormaSeguridad]([IDNormaSeguridad] [int] IDENTITY(1,1)  NOT NULL,
											[NSTitulo] [nvarchar] (50) NOT NULL,
											[Descripcion] [nvarchar] (200) NOT NULL,

		 CONSTRAINT [PK_NormaSeguridad] PRIMARY KEY 
		(
			[IDNormaSeguridad] ASC
		));

		PRINT 'Table NormaSeguridad created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table NormaSeguridad already exists into the database';
	END
GO


PRINT 'Creating the NormaSeguridadItem table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[NormaSeguridadItem]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[NormaSeguridadItem]([IDNormaSeguridadItem] [int] IDENTITY(1,1)  NOT NULL,
												[NSItem] [nvarchar] (300) NOT NULL,
		                                        [IDNormaSeguridad] [int]                   NOT NULL,

		 CONSTRAINT [PK_NormaSeguridadItem] PRIMARY KEY 
		(
			[IDNormaSeguridadItem] ASC
		));

		PRINT 'Table NormaSeguridadItem created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table NormaSeguridadItem already exists into the database';
	END
GO


PRINT 'Creating the Accidente table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[Accidente]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[Accidente]([IDAccidente] [int] IDENTITY(1,1)  NOT NULL,
										[NombreAccidente] [nvarchar] (50) NOT NULL,
										[IDNormaSeguridad] [int]          NOT NULL,

		 CONSTRAINT [PK_Accidente] PRIMARY KEY 
		(
			[IDAccidente] ASC
		));

		PRINT 'Table Accidente created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table Accidente already exists into the database';
	END
GO


PRINT 'Creating the AccidenteOcurrido table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[AccidenteOcurrido]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[AccidenteOcurrido]([IDAccidenteOcurrido] [int] IDENTITY(1,1)  NOT NULL,
												[Fecha] [datetime] NOT NULL,
												[IDAccidente] [int] NOT NULL,
												[IDActividadDetalle] [int],
		 CONSTRAINT [PK_AccidenteOcurrido] PRIMARY KEY 
		(
			[IDAccidenteOcurrido] ASC
		));

		PRINT 'Table AccidenteOcurrido created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table AccidenteOcurrido already exists into the database';
	END
GO


PRINT 'Creating the DetalleAccidenteOcurrido table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[DetalleAccidenteOcurrido]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[DetalleAccidenteOcurrido]([IDDetalleAccidenteOcurrido] [int] IDENTITY(1,1)  NOT NULL,
										              [IDAccidenteOcurrido] [int]                      NOT NULL,
													  [DescripcionAccidenteOcurrido] [nvarchar] (300) NOT NULL, 
		 CONSTRAINT [PK_DetalleAccidenteOcurrido] PRIMARY KEY 
		(
			[IDDetalleAccidenteOcurrido] ASC
		));

		PRINT 'Table DetalleAccidenteOcurrido created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table DetalleAccidenteOcurrido already exists into the database';
	END
GO


PRINT 'Creating the AsignacionAccidenteOcurrido table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[AsignacionAccidenteOcurrido]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[AsignacionAccidenteOcurrido]([IDAsignacionAccidenteOcurrido] [int] IDENTITY(1,1)  NOT NULL,
										             [IDAsignacion] [int]                                    NOT NULL,
                                                     [IDAccidenteOcurrido] [int]                             NOT NULL,
		 CONSTRAINT [PK_AsignacionAccidenteOcurrido] PRIMARY KEY 
		(
			[IDAsignacionAccidenteOcurrido] ASC
		));

		PRINT 'Table AsignacionAccidenteOcurrido created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table AsignacionAccidenteOcurrido already exists into the database';
	END
GO


PRINT 'Creating the ActividadAccidente table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[ActividadAccidente]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[ActividadAccidente]([IDActividadAccidente] [int] IDENTITY(1,1)  NOT NULL,
										        [IDActividad] [int]                        NOT NULL,
                                                [IDAccidente] [int]                        NOT NULL,
		 CONSTRAINT [PK_ActividadAccidente] PRIMARY KEY 
		(
			[IDActividadAccidente] ASC
		));

		PRINT 'Table ActividadAccidente created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table ActividadAccidente already exists into the database';
	END
GO


PRINT 'Creating the NormaSeguridadActividadDetalle table....';

IF NOT EXISTS (SELECT * FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[NormaSeguridadActividadDetalle]') 
		       AND type in (N'U'))
BEGIN
		CREATE TABLE [dbo].[NormaSeguridadActividadDetalle]([IDNormaSeguridadActividadDetalle] [int] IDENTITY(1,1)  NOT NULL,
										                    [IDNormaSeguridad] [int]                               NOT NULL,
                                                            [IDActividadDetalle] [int]                             NOT NULL,
		 CONSTRAINT [PK_NormaSeguridadActividadDetalle] PRIMARY KEY 
		(
			[IDNormaSeguridadActividadDetalle] ASC
		));

		PRINT 'Table NormaSeguridadActividadDetalle created!';
	END
 ELSE 
	BEGIN
		PRINT 'Table NormaSeguridadActividadDetalle already exists into the database';
	END
GO


---DEFINIENDO RELACIONES:


-- Define the relationship between Login and Trabajador.
IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Login_Trabajador]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Login]'))
ALTER TABLE [dbo].[Login]  WITH CHECK ADD  
       CONSTRAINT [FK_Login_Trabajador] FOREIGN KEY([IDTrabajador])
REFERENCES [dbo].[Trabajador] ([IDTrabajador])
GO
ALTER TABLE [dbo].[Login] CHECK 
       CONSTRAINT [FK_Login_Trabajador]
GO

-- Define the relationship between Trabajador and Rol.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Trabajador_Rol]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Trabajador]'))
ALTER TABLE [dbo].[Trabajador]  WITH CHECK ADD  
       CONSTRAINT [FK_Trabajador_Rol] FOREIGN KEY([IDRol])
REFERENCES [dbo].[Rol] ([IDRol])
GO
ALTER TABLE [dbo].[Trabajador] CHECK 
       CONSTRAINT [FK_Trabajador_Rol]
GO

-- Define the relationship between AsignacionFuncion and Rol.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_AsignacionFuncion_Rol]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[AsignacionFuncion]'))
ALTER TABLE [dbo].[AsignacionFuncion]  WITH CHECK ADD  
       CONSTRAINT [FK_AsignacionFuncion_Rol] FOREIGN KEY([IDRol])
REFERENCES [dbo].[Rol] ([IDRol])
GO
ALTER TABLE [dbo].[AsignacionFuncion] CHECK 
       CONSTRAINT [FK_AsignacionFuncion_Rol]
GO

-- Define the relationship between AsignacionFuncion and ManualFunciones.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_AsignacionFuncion_ManualFunciones]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[AsignacionFuncion]'))
ALTER TABLE [dbo].[AsignacionFuncion]  WITH CHECK ADD  
       CONSTRAINT [FK_AsignacionFuncion_ManualFunciones] FOREIGN KEY([IDManualFunciones])
REFERENCES [dbo].[ManualFunciones] ([IDManualFunciones])
GO
ALTER TABLE [dbo].[AsignacionFuncion] CHECK 
       CONSTRAINT [FK_AsignacionFuncion_ManualFunciones]
GO

-- Define the relationship between RolEquipo and Rol.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_RolEquipo_Rol]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[RolEquipo]'))
ALTER TABLE [dbo].[RolEquipo]  WITH CHECK ADD  
       CONSTRAINT [FK_RolEquipo_Rol] FOREIGN KEY([IDRol])
REFERENCES [dbo].[Rol] ([IDRol])
GO
ALTER TABLE [dbo].[RolEquipo] CHECK 
       CONSTRAINT [FK_RolEquipo_Rol]
GO

-- Define the relationship between RolEquipo and Equipo.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_RolEquipo_Equipo]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[RolEquipo]'))
ALTER TABLE [dbo].[RolEquipo]  WITH CHECK ADD  
       CONSTRAINT [FK_RolEquipo_Equipo] FOREIGN KEY([IDEquipo])
REFERENCES [dbo].[Equipo] ([IDEquipo])
GO
ALTER TABLE [dbo].[RolEquipo] CHECK 
       CONSTRAINT [FK_RolEquipo_Equipo]
GO

-- Define the relationship between Equipo and TipoEquipo.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Equipo_TipoEquipo]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Equipo]'))
ALTER TABLE [dbo].[Equipo]  WITH CHECK ADD  
       CONSTRAINT [FK_Equipo_TipoEquipo] FOREIGN KEY([IDTipoEquipo])
REFERENCES [dbo].[TipoEquipo] ([IDTipoEquipo])
GO
ALTER TABLE [dbo].[Equipo] CHECK 
       CONSTRAINT [FK_Equipo_TipoEquipo]
GO

-- Define the relationship between EquipoAsignado and Asignacion.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_EquipoAsignado_Asignacion]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[EquipoAsignado]'))
ALTER TABLE [dbo].[EquipoAsignado]  WITH CHECK ADD  
       CONSTRAINT [FK_EquipoAsignado_Asignacion] FOREIGN KEY([IDAsignacion])
REFERENCES [dbo].[Asignacion] ([IDAsignacion])
GO
ALTER TABLE [dbo].[EquipoAsignado] CHECK 
       CONSTRAINT [FK_EquipoAsignado_Asignacion]
GO

-- Define the relationship between EquipoAsignado and Equipo.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_EquipoAsignado_Equipo]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[EquipoAsignado]'))
ALTER TABLE [dbo].[EquipoAsignado]  WITH CHECK ADD  
       CONSTRAINT [FK_EquipoAsignado_Equipo] FOREIGN KEY([IDEquipo])
REFERENCES [dbo].[Equipo] ([IDEquipo])
GO
ALTER TABLE [dbo].[EquipoAsignado] CHECK 
       CONSTRAINT [FK_EquipoAsignado_Equipo]
GO

-- Define the relationship between ActividadDetalle and Actividad.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_ActividadDetalle_Actividad]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[ActividadDetalle]'))
ALTER TABLE [dbo].[ActividadDetalle]  WITH CHECK ADD  
       CONSTRAINT [FK_ActividadDetalle_Actividad] FOREIGN KEY([IDActividad])
REFERENCES [dbo].[Actividad] ([IDActividad])
GO
ALTER TABLE [dbo].[ActividadDetalle] CHECK 
       CONSTRAINT [FK_ActividadDetalle_Actividad]
GO

-- Define the relationship between ActividadDetalle and Proyecto.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_ActividadDetalle_Proyecto]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[ActividadDetalle]'))
ALTER TABLE [dbo].[ActividadDetalle]  WITH CHECK ADD  
       CONSTRAINT [FK_ActividadDetalle_Proyecto] FOREIGN KEY([IDProyecto])
REFERENCES [dbo].[Proyecto] ([IDProyecto])
GO
ALTER TABLE [dbo].[ActividadDetalle] CHECK 
       CONSTRAINT [FK_ActividadDetalle_Proyecto]
GO

-- Define the relationship between Asignacion and Trabajador.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Asignacion_Trabajador]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Asignacion]'))
ALTER TABLE [dbo].[Asignacion]  WITH CHECK ADD  
       CONSTRAINT [FK_Asignacion_Trabajador] FOREIGN KEY([IDTrabajador])
REFERENCES [dbo].[Trabajador] ([IDTrabajador])
GO
ALTER TABLE [dbo].[Asignacion] CHECK 
       CONSTRAINT [FK_Asignacion_Trabajador]
GO

-- Define the relationship between Asignacion and ActividadDetalle.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Asignacion_ActividadDetalle]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Asignacion]'))
ALTER TABLE [dbo].[Asignacion]  WITH CHECK ADD  
       CONSTRAINT [FK_Asignacion_ActividadDetalle] FOREIGN KEY([IDActividadDetalle])
REFERENCES [dbo].[ActividadDetalle] ([IDActividadDetalle])
GO
ALTER TABLE [dbo].[Asignacion] CHECK 
       CONSTRAINT [FK_Asignacion_ActividadDetalle]
GO

-- Define the relationship between AsignacionAccidenteOcurrido and Asignacion.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_AsignacionAccidenteOcurrido_Asignacion]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[AsignacionAccidenteOcurrido]'))
ALTER TABLE [dbo].[AsignacionAccidenteOcurrido]  WITH CHECK ADD  
       CONSTRAINT [FK_AsignacionAccidenteOcurrido_Asignacion] FOREIGN KEY([IDAsignacion])
REFERENCES [dbo].[Asignacion] ([IDAsignacion])
GO
ALTER TABLE [dbo].[AsignacionAccidenteOcurrido] CHECK 
       CONSTRAINT [FK_AsignacionAccidenteOcurrido_Asignacion]
GO

-- Define the relationship between AsignacionAccidenteOcurrido and AccidenteOcurrido.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_AsignacionAccidenteOcurrido_AccidenteOcurrido]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[AsignacionAccidenteOcurrido]'))
ALTER TABLE [dbo].[AsignacionAccidenteOcurrido]  WITH CHECK ADD  
       CONSTRAINT [FK_AsignacionAccidenteOcurrido_AccidenteOcurrido] FOREIGN KEY([IDAccidenteOcurrido])
REFERENCES [dbo].[AccidenteOcurrido] ([IDAccidenteOcurrido])
GO
ALTER TABLE [dbo].[AsignacionAccidenteOcurrido] CHECK 
       CONSTRAINT [FK_AsignacionAccidenteOcurrido_AccidenteOcurrido]
GO

-- Define the relationship between DetalleAccidenteOcurrido and AccidenteOcurrido.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleAccidenteOcurrido_AccidenteOcurrido]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleAccidenteOcurrido]'))
ALTER TABLE [dbo].[DetalleAccidenteOcurrido]  WITH CHECK ADD  
       CONSTRAINT [FK_DetalleAccidenteOcurrido_AccidenteOcurrido] FOREIGN KEY([IDAccidenteOcurrido])
REFERENCES [dbo].[AccidenteOcurrido] ([IDAccidenteOcurrido])
GO
ALTER TABLE [dbo].[DetalleAccidenteOcurrido] CHECK 
       CONSTRAINT [FK_DetalleAccidenteOcurrido_AccidenteOcurrido]
GO

-- Define the relationship between Accidente and NormaSeguridad.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Accidente_NormaSeguridad]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Accidente]'))
ALTER TABLE [dbo].[Accidente]  WITH CHECK ADD  
       CONSTRAINT [FK_Accidente_NormaSeguridad] FOREIGN KEY([IDNormaSeguridad])
REFERENCES [dbo].[NormaSeguridad] ([IDNormaSeguridad])
GO
ALTER TABLE [dbo].[Accidente] CHECK 
       CONSTRAINT [FK_Accidente_NormaSeguridad]
GO

-- Define the relationship between AccidenteOcurrido and Accidente.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccidenteOcurrido_Accidente]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[AccidenteOcurrido]'))
ALTER TABLE [dbo].[AccidenteOcurrido]  WITH CHECK ADD  
       CONSTRAINT [FK_AccidenteOcurrido_Accidente] FOREIGN KEY([IDAccidente])
REFERENCES [dbo].[Accidente] ([IDAccidente])
GO
ALTER TABLE [dbo].[AccidenteOcurrido] CHECK 
       CONSTRAINT [FK_AccidenteOcurrido_Accidente]
GO

-- Define the relationship between AccidenteOcurrido and ActividadDetalle.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccidenteOcurrido_ActividadDetalle]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[AccidenteOcurrido]'))
ALTER TABLE [dbo].[AccidenteOcurrido]  WITH CHECK ADD  
       CONSTRAINT [FK_AccidenteOcurrido_ActividadDetalle] FOREIGN KEY([IDActividadDetalle])
REFERENCES [dbo].[ActividadDetalle] ([IDActividadDetalle])
GO
ALTER TABLE [dbo].[AccidenteOcurrido] CHECK 
       CONSTRAINT [FK_AccidenteOcurrido_ActividadDetalle]
GO

-- Define the relationship between NormaSeguridadItem and NormaSeguridad.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_NormaSeguridadItem_NormaSeguridad]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[NormaSeguridadItem]'))
ALTER TABLE [dbo].[NormaSeguridadItem]  WITH CHECK ADD  
       CONSTRAINT [FK_NormaSeguridadItem_NormaSeguridad] FOREIGN KEY([IDNormaSeguridad])
REFERENCES [dbo].[NormaSeguridad] ([IDNormaSeguridad])
GO
ALTER TABLE [dbo].[NormaSeguridadItem] CHECK 
       CONSTRAINT [FK_NormaSeguridadItem_NormaSeguridad]
GO

-- Define the relationship between ActividadAccidente and Actividad.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_ActividadAccidente_Actividad]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[ActividadAccidente]'))
ALTER TABLE [dbo].[ActividadAccidente]  WITH CHECK ADD  
       CONSTRAINT [FK_ActividadAccidente_Actividad] FOREIGN KEY([IDActividad])
REFERENCES [dbo].[Actividad] ([IDActividad])
GO
ALTER TABLE [dbo].[ActividadAccidente] CHECK 
       CONSTRAINT [FK_ActividadAccidente_Actividad]
GO

-- Define the relationship between ActividadAccidente and Accidente.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_ActividadAccidente_Accidente]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[ActividadAccidente]'))
ALTER TABLE [dbo].[ActividadAccidente]  WITH CHECK ADD  
       CONSTRAINT [FK_ActividadAccidente_Accidente] FOREIGN KEY([IDAccidente])
REFERENCES [dbo].[Accidente] ([IDAccidente])
GO
ALTER TABLE [dbo].[ActividadAccidente] CHECK 
       CONSTRAINT [FK_ActividadAccidente_Accidente]
GO

-- Define the relationship between NormaSeguridadActividadDetalle and NormaSeguridad.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_NormaSeguridadActividadDetalle_NormaSeguridad]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[NormaSeguridadActividadDetalle]'))
ALTER TABLE [dbo].[NormaSeguridadActividadDetalle]  WITH CHECK ADD  
       CONSTRAINT [FK_NormaSeguridadActividadDetalle_NormaSeguridad] FOREIGN KEY([IDNormaSeguridad])
REFERENCES [dbo].[NormaSeguridad] ([IDNormaSeguridad])
GO
ALTER TABLE [dbo].[NormaSeguridadActividadDetalle] CHECK 
       CONSTRAINT [FK_NormaSeguridadActividadDetalle_NormaSeguridad]
GO

-- Define the relationship between NormaSeguridadActividadDetalle and ActividadDetalle.

IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_NormaSeguridadActividadDetalle_ActividadDetalle]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[NormaSeguridadActividadDetalle]'))
ALTER TABLE [dbo].[NormaSeguridadActividadDetalle]  WITH CHECK ADD  
       CONSTRAINT [FK_NormaSeguridadActividadDetalle_ActividadDetalle] FOREIGN KEY([IDActividadDetalle])
REFERENCES [dbo].[ActividadDetalle] ([IDActividadDetalle])
GO
ALTER TABLE [dbo].[NormaSeguridadActividadDetalle] CHECK 
       CONSTRAINT [FK_NormaSeguridadActividadDetalle_ActividadDetalle]
GO