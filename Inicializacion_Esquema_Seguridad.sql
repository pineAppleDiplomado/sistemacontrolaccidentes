/*
*	Inicializacion Esquema Seguridad  
*	Desc: Inicializacion de datos en tablas
*
*	Nombre					Fecha         Descripcion
*	---------------------------------------------------
*	Ivan Zapata Torrico		08/02/2019    Inicializacion de datos en tablas del Esquema Seguridad
*/

USE Seguridad;

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN TRANSACTION;

--*********** ROL (Catalogo) *************

PRINT 'Insertando datos en la tabla Rol';
SET IDENTITY_INSERT dbo.Rol ON
INSERT INTO dbo.Rol (IDRol, NombreRol)
VALUES (1, 'Supervisor de obra');
INSERT INTO dbo.Rol (IDRol, NombreRol)
VALUES (2, 'Capataz');
INSERT INTO dbo.Rol (IDRol, NombreRol)
VALUES (3, 'Maestro');
INSERT INTO dbo.Rol (IDRol, NombreRol)
VALUES (4, 'Contramaestro');
INSERT INTO dbo.Rol (IDRol, NombreRol)
VALUES (5, 'Ayudante');
INSERT INTO dbo.Rol (IDRol, NombreRol)
VALUES (6, 'Soldador');
INSERT INTO dbo.Rol (IDRol, NombreRol)
VALUES (7, 'Encofrador');
INSERT INTO dbo.Rol (IDRol, NombreRol)
VALUES (8, 'Jefe tecnico');
SET IDENTITY_INSERT dbo.Rol OFF
PRINT 'Tabla Rol listo...';


-- ******** TIPO EQUIPO (Catalogo)**********

PRINT 'Insertando datos en la tabla TipoEquipo';
SET IDENTITY_INSERT dbo.TipoEquipo ON
INSERT INTO dbo.TipoEquipo(IDTipoEquipo, NombreTipoEquipo)
VALUES (1, 'Basico');
INSERT INTO dbo.TipoEquipo(IDTipoEquipo, NombreTipoEquipo)
VALUES (2, 'Logistico');
INSERT INTO dbo.TipoEquipo(IDTipoEquipo, NombreTipoEquipo)
VALUES (3, 'Maquinaria pesada');
SET IDENTITY_INSERT dbo.TipoEquipo OFF
PRINT 'Tabla TipoEquipo listo...';


--************* MANUAL FUNCIONES (Catalogo)***********

PRINT 'Insertando datos en la tabla ManualFunciones';
SET IDENTITY_INSERT dbo.ManualFunciones ON
INSERT INTO dbo.ManualFunciones (IDManualFunciones, Descripcion)
VALUES (1, 'Controlar que se realicen las tareas y trabajos'); --Capataz
INSERT INTO dbo.ManualFunciones (IDManualFunciones, Descripcion)
VALUES (2, 'Verificar el proyecto de la obra. Seguimiento del cronograma. Aprobar los trabajos que se ejecutan. Verificar que se cumplan las normas de seguridad'); --Supervisor de obra
INSERT INTO dbo.ManualFunciones (IDManualFunciones, Descripcion)
VALUES (3, 'Ejecutar la obra de acuerdo con las especificaciones t�cnicas. Control y organizaci�n de los grupos de trabajo'); --Maestro
INSERT INTO dbo.ManualFunciones (IDManualFunciones, Descripcion)
VALUES (4, 'Ejecutar tareas de complejidad media'); --Contra aestro
INSERT INTO dbo.ManualFunciones (IDManualFunciones, Descripcion)
VALUES (5, 'Asesor en el uso de equipo tecnico, insumos, maquinas, herramintas, normas de seguridad'); --Jefe Tecnico
INSERT INTO dbo.ManualFunciones (IDManualFunciones, Descripcion)
VALUES (6, 'Encofrados de madera, met�licos, o mixtos de cualquier otro material, que se emplean para moldear piezas de hormig�n.'); --Encofrador
INSERT INTO dbo.ManualFunciones (IDManualFunciones, Descripcion)
VALUES (7, 'Apoyo en soldadura de piezas metalicas'); --Soldador
INSERT INTO dbo.ManualFunciones (IDManualFunciones, Descripcion)
VALUES (8, 'Apoyo general'); --Ayudante
SET IDENTITY_INSERT dbo.ManualFunciones OFF
PRINT 'Tabla ManualFunciones listo......';


-- ******** PROYECTO (Catalogo) **********

--SET IDENTITY_INSERT dbo.Proyecto ON
PRINT 'Insertando datos en la tabla proyecto';
SET IDENTITY_INSERT dbo.Proyecto ON
INSERT INTO dbo.Proyecto (IDProyecto, NombreProyecto, Descripcion)
VALUES (1, 'Condomio Mediterraneo X', 'Cadena de 12 viviendas en la zona de Sarco');
INSERT INTO dbo.Proyecto (IDProyecto, NombreProyecto, Descripcion)
VALUES (2, 'Edificio Margarita', 'Edificio de 15 pisos en la zona central');
INSERT INTO dbo.Proyecto (IDProyecto, NombreProyecto, Descripcion)
VALUES (3, 'Estructura Metalica', 'Techado de estructura metalica de cancha polifuncional en villa Victoria');
INSERT INTO dbo.Proyecto (IDProyecto, NombreProyecto, Descripcion)
VALUES (4, 'Viviendas Sociales', 'Construccion de 200 viviendas sociales en el valle alto, en la comunidad de Villa Rivero');
INSERT INTO dbo.Proyecto (IDProyecto, NombreProyecto, Descripcion)
VALUES (5, 'Complejo deportivo Olympic', 'Complejo deportivo sobre 500 metros cuadrados en la localidad de Punata');
SET IDENTITY_INSERT dbo.Proyecto OFF
PRINT 'Tabla Proyecto listo...';


-- *********** ACTIVIDAD (Catalogo) **********

PRINT 'Insertando datos en la tabla Actividad';
SET IDENTITY_INSERT dbo.Actividad ON
INSERT INTO dbo.Actividad(IDActividad, NombreActividad)
VALUES (1, 'Encofrado');
INSERT INTO dbo.Actividad(IDActividad, NombreActividad)
VALUES (2, 'Estuqueado');
INSERT INTO dbo.Actividad(IDActividad, NombreActividad)
VALUES (3, 'Revoque');
INSERT INTO dbo.Actividad(IDActividad, NombreActividad)
VALUES (4, 'Vaciado de cimiento');
INSERT INTO dbo.Actividad(IDActividad, NombreActividad)
VALUES (5, 'Apuntalado');
INSERT INTO dbo.Actividad(IDActividad, NombreActividad)
VALUES (6, 'Techado');
INSERT INTO dbo.Actividad(IDActividad, NombreActividad)
VALUES (7, 'Pintura');
INSERT INTO dbo.Actividad(IDActividad, NombreActividad)
VALUES (8, 'Aprobacion de trabajos');
SET IDENTITY_INSERT dbo.Actividad OFF
PRINT 'Tabla Actividad listo...';


--************* TRABAJADOR *************

PRINT 'Insertando datos en la tabla Trabajador';
SET IDENTITY_INSERT dbo.Trabajador ON
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (1, 'Abercrombie', 'Kim', '1995-03-11', 5);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (2, 'Barzdukas', 'Gytis', '2005-09-01', 2);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (3, 'Justice', 'Peggy', '2001-09-01', 6);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (4, 'Fakhouri', 'Fadi', '2002-08-06', 7);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (5, 'Harui', 'Roger', '1998-07-01', 3);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (6, 'Li', 'Yan', '2002-09-01', 5);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (7, 'Norman', 'Laura', '2003-09-01', 1);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (8, 'Olivotto', 'Nino', '2005-09-01', 2);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (9, 'Tang', 'Wayne', '2005-09-01', 3);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (10, 'Alonso', 'Meredith', '2002-09-01', 4);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (11, 'Lopez', 'Sophia', '2004-09-01', 8);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (12, 'Browning', 'Meredith', '2000-09-01', 5);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (13, 'Anand', 'Arturo', '2003-09-01', 4);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (14, 'Walker', 'Alexandra','2000-09-01', 2);
INSERT INTO dbo.Trabajador (IDTrabajador, Apellidos, Nombres, FechaContratacion, IDRol)
VALUES (15, 'Powell', 'Carson', '2004-09-01', 5);
SET IDENTITY_INSERT dbo.Trabajador OFF
PRINT 'Tabla Trabajador listo...';


-- ************* EQUIPO ***********

PRINT 'Insertando datos en la tabla Equipo';
SET IDENTITY_INSERT dbo.Equipo ON
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (1, 'Casco', 1); -- Basico
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (2, 'Amoladora', 2);
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (3, 'Taladro', 2);
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (4, 'Mezcladora de cemento', 3); ---Maq pesada
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (5, 'Guinche', 2); ---Logistico
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (6, 'Vibradora', 3);
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (7, 'Plancha de encofrado', 2);
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (8, 'Botas', 1);
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (9, 'Arnes', 2);
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (10, 'Lineas de vida', 2); 
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (11, 'Chaleco', 1);
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (12, 'Balde', 2);
INSERT INTO dbo.Equipo (IDEquipo, NombreEquipo, IDTipoEquipo)
VALUES (13, 'Gafas protectoras', 2);
SET IDENTITY_INSERT dbo.Equipo OFF
PRINT 'Tabla Equipo listo...';


-- ************ NORMA SEGURIDAD **********

PRINT 'Insertando datos en la tabla NormaSeguridad';
SET IDENTITY_INSERT dbo.NormaSeguridad ON
INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
VALUES (1, 'NS-HERRAMIENTAS', 'No realizar un trabajo si no se cuenta con las herramientas necesarias para llevarlo a cabo con garant�as.')
INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
VALUES (2, 'NS-EQUIPO', 'Usar equipo de protecci�n siempre que el trabajo que se este haciendo lo requiera. Utilizar casco en todas las obras');
INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
VALUES (3, 'NS-CARGAS ELEVADAS', 'No ponerse nunca debajo de una carga elevada ni pasar andando por debajo.');
INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
VALUES (4, 'NS-SISTEMAS MECANICOS', 'Cualquier trabajo que implique el uso de sistemas mec�nicos tales como gr�as o montacargas requiere un an�lisis preliminar del riesgo.');
INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
VALUES (5, 'NS-TRASPORTE DE CARGA', 'Proteja tu espalda. Si necesita recoger y transportar cargas pesadas, mantenga la carga cerca del cuerpo y lev�ntela con los m�sculos de las piernas.');
--INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
--VALUES (6, 'NS-TRABAJO EN ALTURA 1', 'No trabaje en alturas sin un arn�s de seguridad cuando no hay medios de protecci�n colectiva.');
--INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
--VALUES (7, 'NS-TRABAJO EN ALTURA 2', 'Los trabajos en altura se realizan en plataformas fijas o m�viles con una barrera de protecci�n dise�ada para la tarea en cuesti�n.');
INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
VALUES (6, 'NS-TRABAJO EN ALTURA', 'No trabaje en alturas sin tomar las medidas de seguridad necesarias y cuando no hay medios de protecci�n colectiva.');
INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
VALUES (7, 'NS-ALCOHOL DROGAS', 'Nada de alcohol ni drogas.');
INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
VALUES (8, 'NS-INFORME DE RIESGO', 'Es necesario que los trabajadores informen a sus superiores si aprecian cualquier riesgo potencia o anomal�a.');
INSERT INTO dbo.NormaSeguridad(IDNormaSeguridad, NSTitulo, Descripcion)
VALUES (9, 'NS-INFORME DE RIESGO', 'Es necesario que los trabajadores informen a sus superiores si aprecian cualquier riesgo potencia o anomal�a.');
SET IDENTITY_INSERT dbo.NormaSeguridad OFF
PRINT 'Tabla NormaSeguridad listo...';


-- *********** ACCIDENTE **********

PRINT 'Insertando datos en la tabla Accidente';
SET IDENTITY_INSERT dbo.Accidente ON
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (1, 'Caida', 6);
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (2, 'Contusion', 1);
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (3, 'Golpe', 3);
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (4, 'Pisar Clavo', 2);
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (5, 'Descarga electrica', 9);
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (6, 'Cortadura', 2);
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (7, 'Intoxicacion', 8);
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (8, 'Caida', 7);
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (9, 'Caida', 8);
INSERT INTO dbo.Accidente(IDAccidente, NombreAccidente, IDNormaSeguridad)
VALUES (10, 'Contusion', 2);
SET IDENTITY_INSERT dbo.Accidente OFF
PRINT 'Tabla Accidente listo...';


-- ******** ACCIDENTE OCURRIDO **********

PRINT 'Insertando datos en la tabla AccidenteOcurrido';
SET IDENTITY_INSERT dbo.AccidenteOcurrido ON
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (1, '2015-08-06', 4 ); --Pisar Clavo
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (2, '2015-02-15', 6 ); --Cortadura
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (3, '2016-07-18', 3 ); --Golpe
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (4, '2016-03-27', 9 ); --Caida
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (5, '2016-10-30', 1 ); --Caida
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (6, '2017-12-09', 6 );
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (7, '2017-04-11', 5 );
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (8, '2017-05-13', 2 );
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (9, '2017-10-01', 7 );
INSERT INTO dbo.AccidenteOcurrido(IDAccidenteOcurrido, Fecha, IDAccidente)
VALUES (10, '2017-11-08', 8 );
SET IDENTITY_INSERT dbo.AccidenteOcurrido OFF
PRINT 'Tabla AccidenteOcurrido listo...';


-- ********** NORMA SEGURIDAD ITEM ***********

PRINT 'Insertando datos en la tabla NormaSeguridadItem';
SET IDENTITY_INSERT dbo.NormaSeguridadItem ON
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (1, 'El uso de guantes aislantes es necesario para mapipular cables energizados',1); --NSItemA-NS1
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (2, 'Usar herramientas de contacto con material que permita tener un buen agarre', 1);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (3, 'Llevar puesto un casco reglamentario', 2);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (4, 'Llevar puesto un arnes', 6);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (5, 'Armado y uso de plataformas fijas', 6);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (6, 'Armado y uso de plataformas moviles', 6);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (7, 'Armado de barreras de proteccion', 3);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (8, 'Usar cinturones para lumbares', 3);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (9, 'Usar colchas de proteccion en caso de contacto con equipo pesado', 3);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (10, 'Las barandillas deben cerrar el per�metro de trabajo en su totalidad', 8);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (11, 'Para aquellos casos en que no es posible instalar una protecci�n colectiva y se ha de hacer mediante trabajos verticales, el elemento prensor del cuerpo debe ser un cintur�n con arn�s completo, adem�s de tener amarrada la cuerda de seguridad', 4);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (12, 'Las escaleras de tijera deben disponer de cuerda o cadena y de zapatos antideslizantes', 1);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (13, 'Las escalera deber� tener una inclinaci�n m�xima, de tal forma que la longitud  desde el apoyo a la pared sea la cuarta parte de la altura desde el suelo hasta el apoyo superior', 6);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (14, 'Los trabajos a m�s de 3,5 metros de altura desde el punto de operaci�n al suelo, que requieran movimientos o esfuerzos peligrosos se realizar�n con protecciones adecuadas y se emplear� un cintur�n de seguridad', 6);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (15, 'Las plataformas de madera fijadas en el hueco deben estar bien encajadas para que se evite el movimiento al pasar sobre ellas', 4);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (16, 'Las plataformas han de estar ubicadas a 2 o m�s metros de altura', 5);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (17, 'Los carteles de se�alizaci�n en las obras se encuentran en la entrada principal de la obras', 8);
INSERT INTO dbo.NormaSeguridadItem (IDNormaSeguridadItem, NSItem, IDNormaSeguridad)
VALUES (18, 'Es necesario hacer las se�alizaciones en obra de se�al primeros auxilios, se�ales de advertencia, se�ales de obligaci�n', 8);
SET IDENTITY_INSERT dbo.NormaSeguridadItem OFF
PRINT 'Tabla NormaSeguridadItem listo......';


-- ************ ASIGNACION FUNCION **********

PRINT 'Insertando datos en la tabla AsignacionFuncion';
SET IDENTITY_INSERT dbo.AsignacionFuncion ON
INSERT INTO dbo.AsignacionFuncion(IDAsignacionFuncion, IDRol, IDManualFunciones)
VALUES (1, 1, 1);
INSERT INTO dbo.AsignacionFuncion(IDAsignacionFuncion, IDRol, IDManualFunciones)
VALUES (2, 2, 2);
INSERT INTO dbo.AsignacionFuncion(IDAsignacionFuncion, IDRol, IDManualFunciones)
VALUES (3, 3, 3);
INSERT INTO dbo.AsignacionFuncion(IDAsignacionFuncion, IDRol, IDManualFunciones)
VALUES (4, 4, 4);
INSERT INTO dbo.AsignacionFuncion(IDAsignacionFuncion, IDRol, IDManualFunciones)
VALUES (5, 5, 5);
INSERT INTO dbo.AsignacionFuncion(IDAsignacionFuncion, IDRol, IDManualFunciones)
VALUES (6, 6, 6);
INSERT INTO dbo.AsignacionFuncion(IDAsignacionFuncion, IDRol, IDManualFunciones)
VALUES (7, 7, 7);
INSERT INTO dbo.AsignacionFuncion(IDAsignacionFuncion, IDRol, IDManualFunciones)
VALUES (8, 8, 8);
SET IDENTITY_INSERT dbo.AsignacionFuncion OFF
PRINT 'Tabla AsignacionFuncion listo...';


-- ********** ROL EQUIPO ***********

PRINT 'Insertando datos en la tabla RolEquipo';
SET IDENTITY_INSERT dbo.RolEquipo ON
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (1, 4, 1);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (2, 6, 5);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (3, 6, 7);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (4, 3, 4);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (5, 7, 6);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (6, 5, 12);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (7, 5, 11);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (8, 4, 8);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (9, 1, 9);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (10, 5, 10);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (11, 6, 3);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (12, 8, 6);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (13, 7, 5);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (14, 5, 8);
INSERT INTO dbo.RolEquipo (IDRolEquipo, IDRol, IDEquipo)
VALUES (16, 2, 7);
SET IDENTITY_INSERT dbo.RolEquipo OFF
PRINT 'Tabla RolEquipo listo......';


-- *********** ACTIVIDAD DETALLE **********

PRINT 'Insertando datos en la tabla ActividadDetalle';
SET IDENTITY_INSERT dbo.ActividadDetalle ON
INSERT INTO dbo.ActividadDetalle(IDActividadDetalle, IDActividad,IDProyecto)
VALUES (1, 1, 1);
INSERT INTO dbo.ActividadDetalle(IDActividadDetalle, IDActividad,IDProyecto)
VALUES (2, 2, 2);
INSERT INTO dbo.ActividadDetalle(IDActividadDetalle, IDActividad,IDProyecto)
VALUES (3, 3, 3);
INSERT INTO dbo.ActividadDetalle(IDActividadDetalle, IDActividad,IDProyecto)
VALUES (4, 4, 4);
INSERT INTO dbo.ActividadDetalle(IDActividadDetalle, IDActividad,IDProyecto)
VALUES (5, 5, 5);
INSERT INTO dbo.ActividadDetalle(IDActividadDetalle, IDActividad,IDProyecto)
VALUES (6, 1, 1);
INSERT INTO dbo.ActividadDetalle(IDActividadDetalle, IDActividad,IDProyecto)
VALUES (7, 4, 1);
INSERT INTO dbo.ActividadDetalle(IDActividadDetalle, IDActividad,IDProyecto)
VALUES (8, 4, 1);
INSERT INTO dbo.ActividadDetalle(IDActividadDetalle, IDActividad,IDProyecto)
VALUES (9, 8, 1);
SET IDENTITY_INSERT dbo.ActividadDetalle OFF
PRINT 'Tabla ActividadDetalle listo...';


-- *********** ACTIVIDAD ACCIDENTE **********

PRINT 'Insertando datos en la tabla ActividadAccidente';
SET IDENTITY_INSERT dbo.ActividadAccidente ON
INSERT INTO dbo.ActividadAccidente(IDActividadAccidente, IDActividad,IDAccidente)
VALUES (1, 1, 1);
INSERT INTO dbo.ActividadAccidente(IDActividadAccidente, IDActividad,IDAccidente)
VALUES (2, 2, 2);
INSERT INTO dbo.ActividadAccidente(IDActividadAccidente, IDActividad,IDAccidente)
VALUES (3, 3, 3);
INSERT INTO dbo.ActividadAccidente(IDActividadAccidente, IDActividad,IDAccidente)
VALUES (4, 4, 4);
INSERT INTO dbo.ActividadAccidente(IDActividadAccidente, IDActividad,IDAccidente)
VALUES (5, 5, 5);
SET IDENTITY_INSERT dbo.ActividadAccidente OFF
PRINT 'Tabla ActividadAccidente listo...';


-- *********** DETALLE ACCIDENTE OCURRIDO **********

PRINT 'Insertando datos en la tabla DetalleAccidenteOcurrido';
SET IDENTITY_INSERT dbo.DetalleAccidenteOcurrido ON
INSERT INTO dbo.DetalleAccidenteOcurrido(IDDetalleAccidenteOcurrido, DescripcionAccidenteOcurrido, IDAccidenteOcurrido)
VALUES (1, 'Piso clavo que estaba en un tablon y se hizo una herida en el pie',1 );
INSERT INTO dbo.DetalleAccidenteOcurrido(IDDetalleAccidenteOcurrido, DescripcionAccidenteOcurrido, IDAccidenteOcurrido)
VALUES (2, 'Herida de corte en la mano con alambre', 2);
INSERT INTO dbo.DetalleAccidenteOcurrido(IDDetalleAccidenteOcurrido, DescripcionAccidenteOcurrido, IDAccidenteOcurrido)
VALUES (3, 'Golpe en el pie con barillas de hierro', 3);
INSERT INTO dbo.DetalleAccidenteOcurrido(IDDetalleAccidenteOcurrido, DescripcionAccidenteOcurrido, IDAccidenteOcurrido)
VALUES (4, 'Se resbalo del andamio y cayo de 3 metros de altura', 4);
INSERT INTO dbo.DetalleAccidenteOcurrido(IDDetalleAccidenteOcurrido, DescripcionAccidenteOcurrido, IDAccidenteOcurrido)
VALUES (5, 'Se tropezo con una caja de heramientas', 5);
SET IDENTITY_INSERT dbo.DetalleAccidenteOcurrido OFF
PRINT 'Tabla DetalleAccidenteOcurrido listo...';


-- *********** ASIGNACION **********

PRINT 'Insertando datos en la tabla Asignacion';
SET IDENTITY_INSERT dbo.Asignacion ON
INSERT INTO dbo.Asignacion(IDAsignacion, IDActividadDetalle, IDTrabajador)
VALUES (1, 1, 5);
INSERT INTO dbo.Asignacion(IDAsignacion, IDActividadDetalle, IDTrabajador)
VALUES (2, 6, 4);
INSERT INTO dbo.Asignacion(IDAsignacion, IDActividadDetalle, IDTrabajador)
VALUES (3, 7, 6);
INSERT INTO dbo.Asignacion(IDAsignacion, IDActividadDetalle, IDTrabajador)
VALUES (4, 8, 1);
INSERT INTO dbo.Asignacion(IDAsignacion, IDActividadDetalle, IDTrabajador)
VALUES (5, 9, 7);
SET IDENTITY_INSERT dbo.Asignacion OFF
PRINT 'Tabla Asignacion listo...';


-- *********** ASIGNACION ACCIDENTE OCURRIDO **********

PRINT 'Insertando datos en la tabla AsignacionAccidenteOcurrido';
SET IDENTITY_INSERT dbo.AsignacionAccidenteOcurrido ON
INSERT INTO dbo.AsignacionAccidenteOcurrido(IDAsignacionAccidenteOcurrido, IDAsignacion, IDAccidenteOcurrido)
VALUES (1, 1, 1);
INSERT INTO dbo.AsignacionAccidenteOcurrido(IDAsignacionAccidenteOcurrido, IDAsignacion, IDAccidenteOcurrido)
VALUES (2, 2, 4);
INSERT INTO dbo.AsignacionAccidenteOcurrido(IDAsignacionAccidenteOcurrido, IDAsignacion, IDAccidenteOcurrido)
VALUES (3, 4, 3);
SET IDENTITY_INSERT dbo.AsignacionAccidenteOcurrido OFF
PRINT 'Tabla AsignacionAccidenteOcurrido listo...';


-- *********** NORMA SEGURIDAD ACTIVIDAD DETALLE **********

PRINT 'Insertando datos en la tabla NormaSeguridadActividadDetalle';
SET IDENTITY_INSERT dbo.NormaSeguridadActividadDetalle ON
INSERT INTO dbo.NormaSeguridadActividadDetalle(IDNormaSeguridadActividadDetalle, IDNormaSeguridad, IDActividadDetalle)
VALUES (1, 2, 1);
INSERT INTO dbo.NormaSeguridadActividadDetalle(IDNormaSeguridadActividadDetalle, IDNormaSeguridad, IDActividadDetalle)
VALUES (2, 3, 1);
INSERT INTO dbo.NormaSeguridadActividadDetalle(IDNormaSeguridadActividadDetalle, IDNormaSeguridad, IDActividadDetalle)
VALUES (3, 6, 1);

INSERT INTO dbo.NormaSeguridadActividadDetalle(IDNormaSeguridadActividadDetalle, IDNormaSeguridad, IDActividadDetalle)
VALUES (4, 2, 6);
INSERT INTO dbo.NormaSeguridadActividadDetalle(IDNormaSeguridadActividadDetalle, IDNormaSeguridad, IDActividadDetalle)
VALUES (5, 3, 6);
INSERT INTO dbo.NormaSeguridadActividadDetalle(IDNormaSeguridadActividadDetalle, IDNormaSeguridad, IDActividadDetalle)
VALUES (6, 6, 6);

INSERT INTO dbo.NormaSeguridadActividadDetalle(IDNormaSeguridadActividadDetalle, IDNormaSeguridad, IDActividadDetalle)
VALUES (7, 4, 7);
SET IDENTITY_INSERT dbo.NormaSeguridadActividadDetalle OFF
PRINT 'Tabla NormaSeguridadActividadDetalle listo...';


-- *********** LOGIN **********

PRINT 'Insertando datos en la tabla Login';
SET IDENTITY_INSERT dbo.[Login] ON
INSERT INTO dbo.[Login](IDLogin, Usuario, Pass, IDTrabajador)
VALUES (1, 'norman', HASHBYTES('SHA2_512', N'norman123'), 7);
INSERT INTO dbo.[Login](IDLogin, Usuario, Pass, IDTrabajador)
VALUES (2, 'lopez', HASHBYTES('SHA2_512', N'lopez123'), 11);
SET IDENTITY_INSERT dbo.[Login] OFF
PRINT 'Tabla Login listo...';


-- ********** EQUIPO ASIGNADO ***********

PRINT 'Insertando datos en la tabla EquipoAsignado';
SET IDENTITY_INSERT dbo.EquipoAsignado ON
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (1, 1, 1);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (2, 3, 1);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (3, 10, 1);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (4, 7, 1);

INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (5, 1, 2);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (6, 3, 2);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (7, 10, 2);

INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (8, 1, 3);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (9, 2, 3);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (10, 3, 3);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (11, 5, 3);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (12, 6, 3);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (13, 12, 3);

INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (14, 1, 4);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (15, 2, 4);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (16, 3, 4);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (17, 5, 4);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (18, 6, 4);
INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (19, 12, 4);

INSERT INTO dbo.EquipoAsignado (IDEquipoAsignado, IDEquipo, IDAsignacion)
VALUES (20, 1, 5);

SET IDENTITY_INSERT dbo.EquipoAsignado OFF
PRINT 'Tabla EquipoAsignado listo......';

PRINT 'TODAS LAS TABLAS HAN SIDO INICIALIZADAS......';

COMMIT TRANSACTION;