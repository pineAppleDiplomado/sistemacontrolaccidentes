/*
*  Script para la creacion de la logica del negocio mediante procedimientos almacenados 
*  proveyendo accesos de seguridad a usuarios especificos
*
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Job Callisaya        02/10/2019     Implementacion Inicial
*  Rene Copaga          02/10/2019     Implementacion Inicial
*/

USE SeguridadCode
GO

/*
*  Procedimiento Almacenado Insertar Trabajador

*  Observacion: El procedimiento corresponde a un modelo anterior en
*               el cual los ids son generados por IDENTITY(1,1)

*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   06/02/2019     Implementacion Inicial
*/

PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[InsertarTrabajador]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[InsertarTrabajador]
END
GO

CREATE PROCEDURE [dbo].[InsertarTrabajador]
(
	@Apellidos VARCHAR(50),
	@Nombres VARCHAR(50),
	@fecha DATETIME,
	@IdRol INT
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN
    DECLARE @existe int;
    IF(@Nombres = '') begin
        PRINT 'El Nombre no puede estar vacio!';
    end
    ELSE begin
        IF(@Nombres IS NULL) begin
            PRINT 'El Nombre no puede ser nulo!';
        end
        ELSE begin
            IF(@Nombres LIKE '%[0-9]%') begin
                PRINT 'El Nombre no puede contener numeros!';
            end
            ELSE begin

    ------
                IF(@Apellidos = '') begin
                    PRINT 'El Apellido no puede estar vacio!';
                end
                ELSE begin
                    IF(@Apellidos IS NULL) begin
                        PRINT 'El Apellido no puede ser nulo!';
                    end
                    ELSE begin
                        IF(@Apellidos LIKE '%[0-9]%') begin
                            PRINT 'El Apellido no puede contener numeros';
                        end
                        ELSE begin

    ------
                                    IF(@IdRol IS NULL) begin
                                        PRINT 'El rol no puede ser nulo';
                                    end
                                    ELSE begin
                                        IF(@IdRol = '') begin
                                            PRINT 'El rol no puede estar vacio';
                                        end
                                        ELSE begin
                                            IF(@IdRol NOT LIKE '%[0-9]%') begin
                                                PRINT 'El rol solo debe contener numeros';
                                            end
                                            ELSE begin

                                                --ver si existe en Entidad Rol  
                                                SELECT @existe = rol.IDRol
                                                FROM Rol rol
                                                WHERE rol.IDRol = @IdRol

                                                IF(@existe IS NULL) begin
                                                    PRINT 'No existe ese codigo de Rol!';
                                                end
                                                ELSE begin
                                                    INSERT INTO [dbo].[Trabajador](Apellidos, Nombres, FechaContratacion, IDRol)
                                                    VALUES (@Apellidos, @Nombres, @fecha, @IdRol);
                                                    PRINT 'Trabajador insertado exitosamente!'
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
END
GO

GRANT EXECUTE ON OBJECT::[dbo].[InsertarTrabajador] TO [ManagerRole] AS [dbo];
GO
        
PRINT 'Procedure [dbo].[InsertarTrabajador] created';
GO

/*
*  Procedimiento Almacenado Update del Rol de un trabajador

*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   06/02/2019     Implementacion Inicial
*/

PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Update_Rol_De_Trabajador]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[Update_Rol_De_Trabajador]
END
GO

CREATE PROCEDURE [dbo].[Update_Rol_De_Trabajador]
(
	@IdTrabajador INT,
	@IdRol INT
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN
    DECLARE @existe int;
    IF(@IdTrabajador IS NULL) begin
        PRINT 'El Id de Trabajador no puede ser Nulo';
    end
    ELSE begin
        IF(@IdTrabajador = '') begin
            PRINT 'El Id de Trabajador no puede estar vacio';
        end
        ELSE begin
            IF(ISNUMERIC(@IdTrabajador) <> 1) begin
                PRINT 'Tipo de dato id de Trabajador incorrecto, ingrese un entero!';
            end	
            ELSE begin
                --ver si existe el id de trabajador
                SELECT @existe = trabajador.IDTrabajador
                FROM Trabajador trabajador
                WHERE trabajador.IDTrabajador = @IdTrabajador

                IF(@existe IS NULL) begin
                    PRINT 'El trabajador no existe';
                end

                ELSE begin
                    IF(@IdRol IS NULL) begin
                        PRINT 'El Id de rol no puede ser Nulo';
                    end
                    ELSE begin
                        IF(@IdRol = '') begin
                            PRINT 'El Id de rol no puede estar vacio';
                        end
                        ELSE begin
                            IF(ISNUMERIC(@IdRol) <> 1) begin
                                PRINT 'Tipo de dato idRol incorrecto, ingrese un entero!';
                            end

                            ELSE begin

                                --ver si el rol existe en entidad Rol
                                SELECT @existe = rol.IDRol
                                FROM Rol rol
                                WHERE rol.IDRol = @IdRol

                                IF(@existe IS NULL) begin
                                    PRINT 'El codigo de Rol no existe';
                                end
                                ELSE begin
                                    UPDATE [dbo].[Trabajador]
                                    SET  [IDRol] = @IdRol
                                    WHERE [IDTrabajador] = @IdTrabajador
                                    PRINT 'Se actualizo el rol del trabajador!'
                                end
                            end
                        end
                    end
                end
            end
        end
    end
END
GO

GRANT EXECUTE ON OBJECT::[dbo].[Update_Rol_De_Trabajador] TO [ManagerRole] AS [dbo];
GO

PRINT 'Procedure [dbo].[Update_Rol_De_Trabajador] created';
GO

/*
*  Procedimiento Almacenado Asignar un Trabajador a una Actividad en Ejecucion por Proyecto.
*
*
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   06/02/2019     Implementacion Inicial
*/


PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Asignar_Trabajador_a_Actividad]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[Asignar_Trabajador_a_Actividad]
END
GO

CREATE PROCEDURE [dbo].[Asignar_Trabajador_a_Actividad]
(
	@IdTrabajador INT,
	@IdActividad INT,
	@IdProyecto INT
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN
    DECLARE @existe int;
    DECLARE @idDetalle int;

    --verificando id de trabajador: nulo, vacio, numerico, existencia en Entidad Trabajdor

    IF(@IdTrabajador IS NULL) begin
        PRINT 'El Id de trabajador no puede ser Nulo';
    end
    ELSE begin
        IF(@IdTrabajador = '') begin
            PRINT 'El Id de trabajador no puede estar vacio';
        end
        ELSE begin
            IF(ISNUMERIC(@IdTrabajador) <> 1) begin
                PRINT 'Tipo de dato idTrabajador incorrecto, ingrese un entero!';
            end
            ELSE begin

                --ver si existe el Id de trabajdor
                SELECT @existe = trabajador.IDTrabajador
                FROM Trabajador trabajador
                WHERE trabajador.IDTrabajador = @IdTrabajador

                IF(@existe IS NULL) begin
                    PRINT 'No existe el trabajador';
                end
                ELSE begin


                    --verificando id de proyecto: nulo, vacio, numerico, existencia
                    IF(@IdProyecto IS NULL) begin
                        PRINT 'El Id de proyecto no puede ser Nulo';
                    end
                    ELSE begin
                        IF(@IdProyecto = '') begin
                            PRINT 'El Id de proyecto no puede estar vacio';
                        end
                        ELSE begin
                            IF(ISNUMERIC(@IdProyecto) <> 1) begin
                                PRINT 'Tipo de dato idProyecto incorrecto, ingrese un entero!';
                            end
                            ELSE begin

                                --ver si existe el Id de proyecto
                                SELECT @existe = proyecto.[IDProyecto]
                                FROM Proyecto proyecto
                                WHERE proyecto.[IDProyecto] = @IdProyecto

                                IF(@existe IS NULL) begin
                                    PRINT 'No existe el proyecto';
                                end
                                ELSE begin


                                    --verificando Id de actividad: nulo, vacio, numerico, existencia en Entidad ActividadDetalle

                                    IF(@IdActividad IS NULL) begin
                                        PRINT 'El Id de actividad no puede ser Nulo';
                                    end
                                    ELSE begin
                                        IF(@IdActividad = '') begin
                                            PRINT 'El Id de actividad no puede estar vacio';
                                        end
                                        ELSE begin
                                            IF(ISNUMERIC(@IdActividad) <> 1) begin
                                                PRINT 'Tipo de dato idActividad incorrecto, ingrese un entero!';
                                            end
                                            ELSE begin

                                                --ver si existe el Id de la actividad en ActividadDetalle, agarrar su IdDetalle
                                                --e insertar en entidad Asignar.
                                                SELECT @existe = actDetalle.[IDActividad], @idDetalle = actDetalle.[IDActividadDetalle]
                                                FROM ActividadDetalle actDetalle
                                                WHERE actDetalle.[IDActividad] = @IdActividad and
                                                      actDetalle.[IDProyecto] = @IdProyecto

                                                IF(@existe IS NULL) begin
                                                    PRINT 'No existe la actividad';
                                                end
                                                ELSE begin
                                                    INSERT INTO [dbo].[Asignacion]([IDActividadDetalle], [IDTrabajador])
                                                    VALUES (@idDetalle, @IdTrabajador);
                                                    PRINT 'Trabajador asignado a una Actividad y a un 
                                                           Proyecto exitosamente!';
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
END
GO

GRANT EXECUTE ON OBJECT::[dbo].[Asignar_Trabajador_a_Actividad] TO [ManagerRole] AS [dbo];
GO

PRINT 'Procedure [dbo].[Asignar_Trabajador_a_Actividad] created';
GO

/*
*  Procedimiento Almacenado Asignar un equipo a un trabajador que tiene 
                            actividad asignada en Ejecucion.
*
*
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   06/02/2019     Implementacion Inicial
*/

PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Asignar_Equipo_a_Trabajador]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[Asignar_Equipo_a_Trabajador]
END
GO

CREATE PROCEDURE [dbo].[Asignar_Equipo_a_Trabajador]
(
	@IdTrabajador INT,
	@IdEquipo INT,
	@IdActividad INT,
	@IdProyecto INT
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN
    DECLARE @existe INT;
    DECLARE @idDetalle INT;
    DECLARE @IdAsignacion INT


    --verificando id de trabajador: nulo, vacio, numerico, existencia en Entidad Trabajdor
    IF(@IdTrabajador IS NULL) begin
        PRINT 'El Id de trabajador no puede ser Nulo';
    end
    ELSE begin
        IF(@IdTrabajador = '') begin
            PRINT 'El Id de trabajador no puede estar vacio';
        end
        ELSE begin
            IF(ISNUMERIC(@IdTrabajador) <> 1) begin
                PRINT 'Tipo de dato idTrabajador incorrecto, ingrese un entero!';
            end
            ELSE begin

                --ver si existe el Id de trabajdor
                SELECT @existe = trabajador.IDTrabajador
                FROM Trabajador trabajador
                WHERE trabajador.IDTrabajador = @IdTrabajador

                IF(@existe IS NULL) begin
                    PRINT 'No existe el trabajador';
                end
                ELSE begin
                    


                    --verificando id de Equipo: nulo, vacio, numerico, existencia en Entidad Equipo
                    IF(@IdEquipo IS NULL) begin
                        PRINT 'El Id de Equipo no puede ser Nulo';
                    end
                    ELSE begin
                        IF(@IdEquipo = '') begin
                            PRINT 'El Id de Equipo no puede estar vacio';
                        end
                        ELSE begin
                            IF(ISNUMERIC(@IdEquipo) <> 1) begin
                                PRINT 'Tipo de dato idEquipo incorrecto, ingrese un entero!';
                            end
                            ELSE begin

                                --ver si existe el Id de equipo
                                SELECT @existe = equipo.[IDEquipo]
                                FROM Equipo equipo
                                WHERE equipo.[IDEquipo] = @IdEquipo

                                IF(@existe IS NULL) begin
                                    PRINT 'No existe el Equipo';
                                end
                                ELSE begin



                                    --verificando id de proyecto: nulo, vacio, numerico, existencia
                                    IF(@IdProyecto IS NULL) begin
                                        PRINT 'El Id de proyecto no puede ser Nulo';
                                    end
                                    ELSE begin
                                        IF(@IdProyecto = '') begin
                                            PRINT 'El Id de proyecto no puede estar vacio';
                                        end
                                        ELSE begin
                                            IF(ISNUMERIC(@IdProyecto) <> 1) begin
                                                PRINT 'Tipo de dato idProyecto incorrecto, ingrese un entero!';
                                            end
                                            ELSE begin

                                                --ver si existe el Id de proyecto
                                                SELECT @existe = proyecto.[IDProyecto]
                                                FROM Proyecto proyecto
                                                WHERE proyecto.[IDProyecto] = @IdProyecto

                                                IF(@existe IS NULL) begin
                                                    PRINT 'No existe el proyecto';
                                                end
                                                ELSE begin



                                                    --verificando Id de actividad: nulo, vacio, numerico, existencia en Entidad ActividadDetalle

                                                    IF(@IdActividad IS NULL) begin
                                                        PRINT 'El Id de actividad no puede ser Nulo';
                                                    end
                                                    ELSE begin
                                                        IF(@IdActividad = '') begin
                                                            PRINT 'El Id de actividad no puede estar vacio';
                                                        end
                                                        ELSE begin
                                                            IF(ISNUMERIC(@IdActividad) <> 1) begin
                                                                PRINT 'Tipo de dato idActividad incorrecto, ingrese un entero!';
                                                            end
                                                            ELSE begin

                                                                --ver si existe el Id de la actividad en Actividad
                                                                SELECT @existe = act.[IDActividad]
                                                                FROM Actividad act
                                                                WHERE act.[IDActividad] = @IdActividad

                                                                IF(@existe IS NULL) begin
                                                                    PRINT 'No existe la actividad';
                                                                end
                                                                ELSE begin


                                                                    --verificando que el trabajador tenga una actividad asignada en un proyecto
                                                                    SELECT @IdAsignacion = asignacion.IDAsignacion
                                                                    FROM Asignacion asignacion INNER JOIN ActividadDetalle actDetalle
                                                                                               ON (asignacion.IDActividadDetalle = actDetalle.IDActividadDetalle)
                                                                    WHERE asignacion.IDTrabajador = @IdTrabajador and
                                                                          actDetalle.IDActividad = @IdActividad   and
                                                                          actDetalle.IDProyecto = @IdProyecto

                                                                    IF(@IdAsignacion IS NULL) begin
                                                                        PRINT 'No existe una asignacion del trabajador con la actividad';
                                                                    end
                                                                    ELSE begin
                                                                        INSERT INTO [dbo].[EquipoAsignado]([IDEquipo], [IDAsignacion])
                                                                        VALUES (@IdEquipo, @IdAsignacion);
                                                                        PRINT 'Equipo asignado a trabajador en dicha actividad exitosamente!';
                                                                    end
                                                                end
                                                            end
                                                        end
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
END
GO

GRANT EXECUTE ON OBJECT::[dbo].[Asignar_Equipo_a_Trabajador] TO [ManagerRole] AS [dbo];
GO

PRINT 'Procedure [dbo].[Asignar_Equipo_a_Trabajador] created';
GO

/*
*  Procedimiento Almacenado Asignar un trabajador a un accidente ocurrido, con detalle.

*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Javier Zapata Candia   06/02/2019     Implementacion Inicial
*/

PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[Asignar_Trabajador_a_Accidente]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[Asignar_Trabajador_a_Accidente]
END
GO

CREATE PROCEDURE [dbo].[Asignar_Trabajador_a_Accidente]
(
	@IdTrabajador INT,
	@IdAccidente INT
	--@detalleAccidente varchar(250)
)
AS

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN
    DECLARE @existe INT;
    DECLARE @IdAsignacion INT;
    DECLARE @IdGeneradoAccidenteOcurrido INT; 

    --verificando id de trabajador: nulo, vacio, numerico, existencia en Entidad Trabajdor
    IF(@IdTrabajador IS NULL) begin
        PRINT 'El Id de trabajador no puede ser Nulo';
    end
    ELSE begin
        IF(@IdTrabajador = '') begin
            PRINT 'El Id de trabajador no puede estar vacio';
        end
        ELSE begin
            IF(ISNUMERIC(@IdTrabajador) <> 1) begin
                PRINT 'Tipo de dato idTrabajador incorrecto, ingrese un entero!';
            end
            ELSE begin

                --ver si existe el Id de trabajdor asignado a una tarea
                SELECT @existe = asignacion.IDTrabajador
                FROM Asignacion asignacion
                WHERE asignacion.IDTrabajador = @IdTrabajador

                IF(@existe IS NULL) begin
                    PRINT 'No existe el trabajador asignado a una tarea!';
                end
                ELSE begin
                    --verificando Id de accidente: nulo, vacio, numerico, existencia en Entidad Accidente

                            IF(@IdAccidente IS NULL) begin
                                PRINT 'El Id de accidente no puede ser Nulo';
                            end
                            ELSE begin
                                IF(@IdAccidente = '') begin
                                    PRINT 'El Id de accidente no puede estar vacio';
                                end
                                ELSE begin
                                    IF(ISNUMERIC(@IdAccidente) <> 1) begin
                                        PRINT 'Tipo de dato idAccidente incorrecto, ingrese un entero!';
                                    end
                                    ELSE begin

                                        --ver si existe el Id de la accidente en entidad Accidente
                                        SELECT @existe = accidente.IDAccidente
                                        FROM Accidente accidente
                                        WHERE accidente.IDAccidente = @IdAccidente

                                        IF(@existe IS NULL) begin
                                            PRINT 'No existe el IdAccidente en la entidad Accidentes!';
                                        end
                                        ELSE begin


                                            /*INSERTANDO A 3 ENTIDADES, AccidenteOcurrido,
                                                                        DetalleAccidenteOcurrido y 
                                                                        AssignacionAccidenteOcurrido
                                            */

                                            --Entidad AccidenteOcurrido
                                                INSERT INTO [dbo].[AccidenteOcurrido]([IDAccidente])
                                                VALUES (@IdAccidente)
                                                SET @IdGeneradoAccidenteOcurrido = @@IDENTITY;

                                            --Entidad DetalleAccidenteOcurrido
                                                --INSERT INTO [dbo].[DetalleAccidenteOcurrido]([IDAccidenteOcurrido],[detalleDelAccidente])
                                                --VALUES (@IdGeneradoAccidenteOcurrido, @detalleAccidente);
                                                INSERT INTO [dbo].[DetalleAccidenteOcurrido]([IDAccidenteOcurrido])
                                                VALUES (@IdGeneradoAccidenteOcurrido);

                                            --Entidad AssignacionAccidenteOcurrido	    --SUPONIENDO QUE TRABAJADOR SOLO TIENE UNA ASIGNACION DE TRABAJO. BANDERA PARA TENER OTRAS.
                                                SELECT @IdAsignacion = asignacion.IDAsignacion
                                                FROM Asignacion asignacion
                                                WHERE asignacion.IDTrabajador = @IdTrabajador

                                                INSERT INTO [dbo].[AsignacionAccidenteOcurrido]([IDAsignacion],[IDAccidenteOcurrido])
                                                VALUES (@IdAsignacion, @IdGeneradoAccidenteOcurrido);
                                                PRINT 'Se asigno el accidente y su detalle al trabajador';
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
END
GO

GRANT EXECUTE ON OBJECT::[dbo].[Asignar_Equipo_a_Trabajador] TO [ManagerRole] AS [dbo];
GO
        
PRINT 'Procedure [dbo].[Asignar_Trabajador_a_Accidente] created';
GO
