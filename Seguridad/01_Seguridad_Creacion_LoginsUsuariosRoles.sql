/*
*  Script para añadir logins, usuarios y roles del sistema de seguridad
*
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Job Callisaya        02/10/2019     Implementacion Inicial
*  Rene Copaga          02/10/2019     Implementacion Inicial
*/

USE [master]
GO

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN TRANSACTION;

/*
    Create logins
*/
PRINT 'Creating Logins....';

:SETVAR passwd "Control123"
IF NOT EXISTS ( SELECT * FROM sys.server_principals WHERE name = 'Manager' )
  CREATE LOGIN [Manager] WITH PASSWORD = '$(passwd)', CHECK_POLICY = OFF;

IF NOT EXISTS ( SELECT * FROM sys.server_principals WHERE name = 'Reader' )
  CREATE LOGIN [Reader] WITH PASSWORD = '$(passwd)', CHECK_POLICY = OFF;


USE [Seguridad]
GO
/*
    Creating users
*/  
PRINT 'Creating Users....';

IF NOT EXISTS ( SELECT 1 FROM sys.database_principals WHERE name = 'Manager')
	CREATE USER [Manager] FOR LOGIN [Manager] WITH DEFAULT_SCHEMA = [dbo];

IF NOT EXISTS ( SELECT 1 FROM sys.database_principals WHERE name = 'Reader')
	CREATE USER [Reader] FOR LOGIN [Reader] WITH DEFAULT_SCHEMA = [dbo];

/*
    Creating roles
*/
PRINT 'Creating Roles.......';

IF NOT EXISTS ( SELECT 1 FROM sys.database_principals WHERE name = 'ManagerRole' and Type = 'R')
	CREATE ROLE ManagerRole AUTHORIZATION [dbo];

IF NOT EXISTS ( SELECT 1 FROM sys.database_principals WHERE name = 'ReaderRole' and Type = 'R')
	CREATE ROLE ReaderRole AUTHORIZATION [dbo];

/*
    Adding permissions to roles
*/
PRINT 'Adding Permissions to Roles.......';
GRANT SELECT ON SCHEMA::[dbo] TO [ManagerRole];
GRANT INSERT ON SCHEMA::[dbo] TO [ManagerRole];
GRANT UPDATE ON SCHEMA::[dbo] TO [ManagerRole];
GRANT DELETE ON SCHEMA::[dbo] TO [ManagerRole];
GO
GRANT SELECT ON SCHEMA::[dbo] TO [ReaderRole];
GO

PRINT 'Adding Users to Roles.......';
ALTER ROLE ManagerRole ADD MEMBER [Manager];
ALTER ROLE ReaderRole ADD MEMBER [Reader];
GO

COMMIT TRANSACTION;