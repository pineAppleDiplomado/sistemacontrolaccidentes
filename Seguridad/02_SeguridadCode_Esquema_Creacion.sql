/*
*  Script para creacion de esquema de base de datos SeguridadCode
*
*  Nombre                 Fecha             Description
*  --------------------------------------------------------
*  Job Callisaya        02/10/2019     Implementacion Inicial
*  Rene Copaga          02/10/2019     Implementacion Inicial
*/

USE SeguridadCode
GO

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN TRANSACTION;

PRINT 'Creating Users....';

IF NOT EXISTS ( SELECT 1 FROM sys.database_principals WHERE name = 'Manager')
	CREATE USER [Manager] FOR LOGIN [Manager] WITH DEFAULT_SCHEMA = [dbo];

IF NOT EXISTS ( SELECT 1 FROM sys.database_principals WHERE name = 'Reader')
	CREATE USER [Reader] FOR LOGIN [Reader] WITH DEFAULT_SCHEMA = [dbo];

PRINT 'Creating Roles.......';

IF NOT EXISTS ( SELECT 1 FROM sys.database_principals WHERE name = 'ManagerRole' and Type = 'R')
	CREATE ROLE ManagerRole AUTHORIZATION [dbo];

IF NOT EXISTS ( SELECT 1 FROM sys.database_principals WHERE name = 'ReaderRole' and Type = 'R')
	CREATE ROLE ReaderRole AUTHORIZATION [dbo];

PRINT 'Adding Users to Roles.......';
ALTER ROLE ManagerRole ADD MEMBER [Manager];
ALTER ROLE ReaderRole ADD MEMBER [Reader];

PRINT 'Creating Synonyms....';

IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'Login')
	CREATE SYNONYM [dbo].[Login] FOR [SchoolData].[dbo].[Login];

IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'Trabajador')
	CREATE SYNONYM [dbo].[Trabajador] FOR [SchoolData].[dbo].[Trabajador];

IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'Rol')
	CREATE SYNONYM [dbo].[Rol] FOR [SchoolData].[dbo].[Rol];

IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'ManualFunciones')
	CREATE SYNONYM [dbo].[ManualFunciones] FOR [SchoolData].[dbo].[ManualFunciones];

IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'AsignacionFuncion')
	CREATE SYNONYM [dbo].[AsignacionFuncion] FOR [SchoolData].[dbo].[AsignacionFuncion];

IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'TipoEquipo')
	CREATE SYNONYM [dbo].[TipoEquipo] FOR [SchoolData].[dbo].[TipoEquipo];

IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'Equipo')
	CREATE SYNONYM [dbo].[Equipo] FOR [SchoolData].[dbo].[Equipo];

IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'RolEquipo')
	CREATE SYNONYM [dbo].[RolEquipo] FOR [SchoolData].[dbo].[RolEquipo];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'Proyecto')
    CREATE SYNONYM [dbo].[Proyecto] FOR [SchoolData].[dbo].[Proyecto];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'Actividad')
    CREATE SYNONYM [dbo].[Actividad] FOR [SchoolData].[dbo].[Actividad];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'ActividadDetalle')
    CREATE SYNONYM [dbo].[ActividadDetalle] FOR [SchoolData].[dbo].[ActividadDetalle];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'Asignacion')
    CREATE SYNONYM [dbo].[Asignacion] FOR [SchoolData].[dbo].[Asignacion];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'EquipoAsignado')
    CREATE SYNONYM [dbo].[EquipoAsignado] FOR [SchoolData].[dbo].[EquipoAsignado];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'NormaSeguridad')
    CREATE SYNONYM [dbo].[NormaSeguridad] FOR [SchoolData].[dbo].[NormaSeguridad];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'NormaSeguridadItem')
    CREATE SYNONYM [dbo].[NormaSeguridadItem] FOR [SchoolData].[dbo].[NormaSeguridadItem];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'Accidente')
    CREATE SYNONYM [dbo].[Accidente] FOR [SchoolData].[dbo].[Accidente];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'AccidenteOcurrido')
    CREATE SYNONYM [dbo].[AccidenteOcurrido] FOR [SchoolData].[dbo].[AccidenteOcurrido];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'DetalleAccidenteOcurrido')
    CREATE SYNONYM [dbo].[DetalleAccidenteOcurrido] FOR [SchoolData].[dbo].[DetalleAccidenteOcurrido];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'AsignacionAccidenteOcurrido')
    CREATE SYNONYM [dbo].[AsignacionAccidenteOcurrido] FOR [SchoolData].[dbo].[AsignacionAccidenteOcurrido];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'ActividadAccidente')
    CREATE SYNONYM [dbo].[ActividadAccidente] FOR [SchoolData].[dbo].[ActividadAccidente];
    
IF NOT EXISTS ( SELECT 1 FROM sys.synonyms WHERE name = 'NormaSeguridadActividadDetalle')
    CREATE SYNONYM [dbo].[NormaSeguridadActividadDetalle] FOR [SchoolData].[dbo].[NormaSeguridadActividadDetalle];
        
COMMIT TRANSACTION;